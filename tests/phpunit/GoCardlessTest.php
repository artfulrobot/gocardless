<?php

use Civi\Test\HeadlessInterface;
use Civi\Test\HookInterface;
use Civi\Test\TransactionalInterface;
use Civi\Api4\PaymentprocessorWebhook;
use CRM_GoCardless_ExtensionUtil as E;

/**
 * Tests the GoCardless direct debit extension.
 *
 * Tips:
 *  - With HookInterface, you may implement CiviCRM hooks directly in the test class.
 *    Simply create corresponding functions (e.g. "hook_civicrm_post(...)" or similar).
 *  - With TransactionalInterface, any data changes made by setUp() or test****() functions will
 *    rollback automatically -- as long as you don't manipulate schema or truncate tables.
 *    If this test needs to manipulate schema or truncate tables, then either:
 *       a. Do all that using setupHeadless() and Civi\Test.
 *       b. Disable TransactionalInterface, and handle all setup/teardown yourself.
 *
 * @group headless
 */
class GoCardlessTest extends PHPUnit\Framework\TestCase implements HeadlessInterface, HookInterface, TransactionalInterface {

  protected $defaultDate = '2021-01-01';
  protected $defaultDateSubscriptionStarts = '2021-01-08';
  /**
   * @var array Holds a map of name -> value for contribution recur statuses */
  protected $contribution_recur_status_map;
  /**
   * @var array Holds a map of name -> value for contribution statuses */
  protected $contribution_status_map;
  /**
   * @var array Holds a map of name -> value for membership statuses
   */
  protected $membership_status_map;
  protected $membership_status_map_flip;

  protected $handleRedirectFlowHookTest;
  protected $hookWasCalled = FALSE;
  protected $contactID = null;
  protected $bugs = [
    'orderCreateSetsMembershipPending' => [
      'applies' => NULL,
      'description' => 'Civi 5.39 - 5.41 incorrectly sets membership status to Pending when one is referenced in Order.create. This is not our fault. Core PR 20976 should fix this. https://github.com/civicrm/civicrm-core/pull/20976',
    ],
    'coreIssue2791' => [
      'applies' => NULL,
      'description' => 'Payment.create wrongly affects membership start_date https://lab.civicrm.org/dev/core/-/issues/2791',
    ]

  ];
  /**
   * Holds test mode payment processor.
   * @var array
   */
  public $test_mode_payment_processor;

  public function setUpHeadless() {
    // Civi\Test has many helpers, like install(), uninstall(), sql(), and sqlFile().
    // See: https://github.com/civicrm/org.civicrm.testapalooza/blob/master/civi-test.md
    // confusingly, unset the following 'reInstallOnce' var, so the install only happens once.
    // Set it to TRUE if you DON'T want to reinstall
    static $reInstallOnce = TRUE;

    $reInstall = FALSE;
    if (!isset($reInstallOnce)) {
      $reInstallOnce=TRUE;
      $reInstall = TRUE;
    }
    return \Civi\Test::headless()
      ->installMe(__DIR__)
      ->install('mjwshared')
      ->apply($reInstall);
  }

  public function setUp() :void {
    parent::setUp();

    $civiVersion = CRM_Utils_System::version();
    $this->bugs['orderCreateSetsMembershipPending']['applies'] = (version_compare($civiVersion, '5.39.0', '>=') && version_compare($civiVersion, '5.42.0', '<'));
    $this->bugs['coreIssue2791']['applies'] = (version_compare($civiVersion, '5.40.0', '>=') && version_compare($civiVersion, '5.42.0', '<'));

    // Set up a Payment Processor that uses GC.

    $this->test_mode_payment_processor = \Civi\Api4\PaymentProcessor::create(FALSE)
      ->setValues([
        'payment_processor_type_id:name' => "GoCardless",
        'name' => "GoCardless",
        'description' => "Set up by test script",
        'signature' => "mock_webhook_key",
        'is_active' => 1,
        'is_test' => 1,
        'url_api' => 'https://api-sandbox.gocardless.com/',
        'user_name' => "fake_test_api_key",
        'payment_instrument_id:name' => "direct_debit_gc",
        'domain_id' => 1,
      ])
      ->execute()->first();

    // We need a live one, too, even though we don't use it.
    \Civi\Api4\PaymentProcessor::create(FALSE)
      ->setValues([
      'payment_processor_type_id:name' => "GoCardless",
      'name' => "GoCardless",
      'signature' => "this is no the webhook key you are looking fo",
      'description' => "Set up by test script",
      'is_active' => 1,
      'url_api' => 'https://api.gocardless.com/',
      'is_test' => 0,
      'user_name' => "fake_live_api_key",
      'payment_instrument_id:name' => "direct_debit_gc",
      'domain_id' => 1,
    ])->execute();

    // Map contribution statuses to values.
    $this->contribution_recur_status_map = array_flip(CRM_Contribute_BAO_ContributionRecur::buildOptions('contribution_status_id', 'validate'));
    $this->contribution_status_map = array_flip(CRM_Contribute_BAO_Contribution::buildOptions('contribution_status_id', 'validate'));

    // Create a membership type
    $result = civicrm_api3('MembershipType', 'create', [
      'member_of_contact_id' => 1,
      'financial_type_id' => "Member Dues",
      'duration_unit' => "year",
      'duration_interval' => 1,
      'period_type' => "rolling",
      'name' => "MyMembershipType",
      'minimum_fee' => 50,
      'auto_renew' => 1,
    ]);

    $this->membership_status_map_flip = CRM_Member_PseudoConstant::membershipstatus();
    $this->membership_status_map = array_flip($this->membership_status_map_flip);

    /** @var \Civi\GoCardless\Log $logger */
    $logger = Civi::log('GoCardless');
    $logger->enableTesting();
  }

  /**
   */
  public function testLog() {
    // Clear log file.
    $logger = Civi::log('GoCardless');
    $logFile = $logger->logFile;
    file_put_contents($logFile, '');
    // print "Log file: $logFile\n";

    $params = ['name' => 'Wilma', 'adjective' => 'nice', 'bag' => 'gage'];
    $logger->info('Hiya {name} how you doing, I like the name {name} it’s very {adjective}', $params);
    $logContent = file_get_contents($logFile);

    /*
    $e = new \InvalidArgumentException("erm");
    $logger->info('Hiya {name} how you doing', ['exception' => $e]);
     */

    // print "\nlog:\n$logContent\n\n";

    $dateRegex = "[0-9T:+-]{25}";
    $this->assertRegexp("/$dateRegex/", $logContent);
    $logContent = substr($logContent, 26);

    // Check that {placeholders} are swapped out; that all context is dumped.
    $this->assertEquals( <<<TXT
      [info] Hiya "Wilma" how you doing, I like the name "Wilma" it’s very "nice"
      {
          "name": "Wilma",
          "adjective": "nice",
          "bag": "gage"
      }

      TXT,
      $logContent);

    // Test labels
    file_put_contents($logFile, '');
    // print "Log file: $logFile\n";

    $logger->info('Line 1');
    $logger->info('Line 2', ['=start' => 'group1']);
    $logger->info('Line 3');
    $logger->info('Line 4', ['=start' => 'group2']);
    $logger->info('Line 5');
    $logger->info('Line 6 - pop should happen after', ['=pop' => 1]);
    $logger->info('Line 7');
    // Calling blank should not result in anything being logged.
    $logger->info('');
    // Calling blank with special context commands should not result in anything being logged.
    // (This next line sets the group to the same as it already is)
    $logger->info('', ['=set' => ['group1']]);
    $logger->info('Line 8', ['=pop' => 1]);
    $logger->info('Line 9');
    $logger->info('Line 10', ['=set' => ['group3', 'group4']]);
    $logger->info('', ['=pop' => 99]); // if only pop, nothing should be logged
    $logger->info('Line 11');
    $logContent = preg_replace("/^$dateRegex \[info\] /m", '', file_get_contents($logFile));
    // print $logContent;

    $this->assertEquals( <<<TXT
      Line 1
      :group1: Line 2
      :group1: Line 3
      :group1:group2: Line 4
      :group1:group2: Line 5
      :group1:group2: Line 6 - pop should happen after
      :group1: Line 7
      :group1: Line 8
      Line 9
      :group3:group4: Line 10
      Line 11

      TXT,
      $logContent);


  }

  /**
   * Test quirks in Civi
   *
   * 5.26: Payment.create did not set trxn_id on Contrib.
   * 5.27: Payment.create DID set trxn_id on Contrib. but receive_date is wrong.
   * 5.28.4: Payment.create sets both receive_date and trxn_id :-)
   * 5.29.1: Payment.create sets trxn_id on Contrib, but does not update receive_date.
   * 5.30: ditto
   * 5.31: ditto
   * 5.35: ditto
   *
   * Payment.create did not used to set the trxn_id apparently.
   */
  public function testPaymentCreate() {
    $contactID = $this->createTestContact();

    // Crete a Pending contrib.
    $contribution = [
      'total_amount'           => '1',
      'receive_date'           => '2021-01-01 09:09:09',
      'financial_type_id'      => 1,
      //'trxn_id'                => 'orig_trxn_id',
      'contact_id'             => $contactID,
      'contribution_status_id' => 'Pending',
      'is_email_receipt'       => FALSE,
    ];
    $createdID = civicrm_api3('Contribution', 'create', $contribution)['id'];

    // Record payment
    civicrm_api3('Payment', 'create', [
      'contribution_id'                   => $createdID,
      'total_amount'                      => '1',
      'is_send_contribution_notification' => FALSE,
      // Updated date
      'trxn_date'                         => '2021-02-02 10:10:10',
      // Updated ID
      'trxn_id'                           => 'my_trxn_id',
    ]);

    // A copy of the method used in CRM_Core_Payment_GoCardlessIPN::doPaymentsConfirmed
    $sql = 'UPDATE civicrm_contribution SET receive_date="%2" WHERE id=%1';
    $sqlParams = [
      1 => [$createdID, 'Positive'],
      2 => [CRM_Utils_Date::isoToMysql('2021-02-02 10:10:10'), 'Date']
    ];
    CRM_Core_DAO::executeQuery($sql, $sqlParams);

    // Fetch the contrib.
    $contrib = civicrm_api3('Contribution', 'get', ['id' => $createdID])['values'][$createdID];

    // Test that Civi updated trxn_id - it should from 5.27, so this tests for regressions.
    $this->assertEquals('my_trxn_id', $contrib['trxn_id'] ?? NULL);
    // Test that the SQL updated the date correctly. Bit superfluous really.
    $this->assertEquals('2021-02-02 10:10:10', $contrib['receive_date'], 'Payment did not change the receive_date');

  }
  /**
   * Check that create redirect flow correctly stores data on session and returns a URL.
   */
  public function testCreateRedirectFlow() {
    // Mock the GC API to return a redirect flow ID and a URL.
    $apiMock = $this->createMock(\GoCardlessPro\Client::class);
    $redirectFlowsService = $this->createMock(\GoCardlessPro\Services\RedirectFlowsService::class);
    $apiMock->method('redirectFlows')->willReturn($redirectFlowsService);

    $redirectFlowsService
      ->expects($this->once())
      ->method('create')
      ->willReturn(json_decode('{"redirect_url":"https://gocardless.com/somewhere","id":"RE1234"}'));

    $pp = $this->test_mode_payment_processor;
    $obj = new CRM_Core_Payment_GoCardless('test', $pp);
    $params = [
      'qfKey' => 'aabbccdd',
      'contactID' => 111,
      'description' => 'test contribution',
      'contributionID' => 222,
      'contributionRecurID' => 333,
      'payment_processor_id' => $this->test_mode_payment_processor['id'],
      'entryURL' => 'http://example.com/somwhere',
    ];
    $obj->setGoCardlessApi($apiMock);

    // The thing we want to test
    $url = $obj->createRedirectFlow($params, 'contribute');

    // Check results.
    $this->assertIsString('string', $url);
    $this->assertNotEmpty($url);
    $this->assertEquals("https://gocardless.com/somewhere", $url);

    // Check inputs for the next stage are stored on the session.
    $sesh = CRM_Core_Session::singleton();
    $sesh_store = $sesh->get('redirect_flows', 'GoCardless');
    $this->assertArrayHasKey('RE1234', $sesh_store);
    $this->assertEquals(TRUE, $sesh_store['RE1234']['test_mode']);
    $this->assertEquals($pp['id'], $sesh_store['RE1234']['payment_processor_id']);
    $this->assertEquals('test contribution', $sesh_store['RE1234']['description']);
    $this->assertEquals(111, $sesh_store['RE1234']['contactID']);
    $this->assertEquals(222, $sesh_store['RE1234']['contributionID']);
    $this->assertEquals(333, $sesh_store['RE1234']['contributionRecurID']);
  }

  /**
   * Check that create redirect flow correctly stores data on session and returns a URL.
   */
  public function testGetRedirectParametersFromParams() {
    $pp = $this->test_mode_payment_processor;
    $obj = new CRM_Core_Payment_GoCardless('test', $pp);
    $billing = CRM_Core_BAO_LocationType::getBilling();
    $params = [
      'qfKey' => 'aabbccdd',
      'contactID' => 111,
      'description' => 'test contribution',
      'contributionID' => 222,
      'contributionRecurID' => 333,
      'payment_processor_id' => $this->test_mode_payment_processor['id'],
      'entryURL' => 'http://example.com/somwhere',
      'first_name' => 'Wilma',
      'last_name' => 'Flintstone',
      'email-Primary' => 'wilma@example.org',
      "street_address-$billing" => '1 The Street',
      "postal_code-$billing" => 'SW1A 0AA',
      "country-$billing" => '',
      'street_address-Primary' => '1 The Road',
      'city-Primary' => 'City Duo',
      'postal_code-Primary' => 'AA12 9ZZ',
    ];

    // The thing we want to test
    $result = $obj->getRedirectParametersFromParams($params, 'contribute');

    $this->assertEquals([
        'given_name' => 'Wilma',
        'family_name' => 'Flintstone',
        'address_line1' => '1 The Street',
        // Nb. there is no billing city, so here we're testing that we don't mix in the primary one!
        'postal_code' => 'SW1A 0AA',
    ], $result['prefilled_customer']);

    // Now test various country inputs.
    $isoCodes = array_flip(CRM_Core_PseudoConstant::countryIsoCode());
    $gbID = $isoCodes['GB'];

    // Valid code for GB should work.
    $result = $obj->getRedirectParametersFromParams(["country-$billing" => $gbID] + $params, 'contribute');
    $this->assertEquals('GB', $result['prefilled_customer']['country_code'] ?? NULL);

    // False, null, empty string, 0 should all not result in a country_code
    foreach ([null, '', FALSE, 0] as $countryID) {
      $result = $obj->getRedirectParametersFromParams(["country-$billing" => $countryID] + $params, 'contribute');
      $this->assertArrayNotHasKey('country_code', $result['prefilled_customer'] ?? []);
    }
  }

  /**
   * This creates a contact with a contribution and a ContributionRecur in the
   * same way that CiviCRM's core Contribution Pages form does, then, having
   * mocked the GC API it calls
   * CRM_GoCardlessUtils::completeRedirectFlowCiviCore()
   * and checks that the result.
   *
   * We expect that the ContributionRecur
   * - is changed to In Progress
   * - has a start_date set to a value returned by the GC API
   * We expect the Contribution record to
   * - still be Pending
   * - have a receive_date set.
   *
   * @dataProvider dataForTestCompleteRedirectFlowCiviCore
   *
   * @param int $installments
   * @param mixed $membership FALSE|new
   */
  public function testCompleteRedirectFlowCiviCore(int $installments, $membershipTest = FALSE) {

    // Create the fixtures
    $dateFormSubmitted = '2021-01-01';
    $dateSubscriptionStarts = '2021-01-08';
    $fixtureOverrides = [
      'recur' => ['start_date'   => $dateFormSubmitted],
      'order' => ['receive_date' => $dateFormSubmitted],
    ];
    if ($membershipTest) {
      $fixtureOverrides['membership'] = [];
    }
    list($contactID, $recur, $contrib, $membership) = $this->createFixture($fixtureOverrides, FALSE);
    $this->mockGoCardlessApiForTestPaymentProcessor1($contactID, $installments, $recur, $dateSubscriptionStarts);

    // Call the thing we're testing with test params.
    $params = [
      'test_mode' => TRUE,
      'redirect_flow_id' => 'RE1234',
      'session_token' => 'aabbccdd',
      'description' => 'test contribution',
      'contactID' => $contactID,
      'contributionID' => $contrib['id'],
      'contributionRecurID' => $recur['id'],
      'payment_processor_id' => $this->test_mode_payment_processor['id'],
      'entryURL' => 'http://example.com/somwhere',
    ];
    if ($installments) {
      $params['installments'] = $installments;
    }
    if ($membershipTest) {
      $params['membershipID'] = $membership['id'];
    }
    CRM_GoCardlessUtils::completeRedirectFlowCiviCore($params);

    //
    // Now test the ContributionRecur record
    //
    $recur = civicrm_api3('ContributionRecur', 'getsingle', ['id' => $recur['id']]);
    $this->assertContributionRecurStatus($recur, 'In Progress');
    // Use processor_id for subscription ID (used to be trxn_id too, but deprecated since 5.48)
    $this->assertEquals('SUBSCRIPTION_ID', $recur['processor_id']);
    // The ContributionRecur's start date should have been updated with the value from the API
    $this->assertEquals("$dateSubscriptionStarts 00:00:00", $recur['start_date']);

    // Now test the contribution's receive date, and check it's still Pending (2)
    $result = civicrm_api3('Contribution', 'getsingle', ['id' => $contrib['id']]);
    $this->assertEquals("$dateSubscriptionStarts 00:00:00", $result['receive_date']);
    $this->assertContributionStatus($result, 'Pending');

    if ($membershipTest) {
      $result = civicrm_api3('Membership', 'getsingle', ['id' => $membership['id']]);
      // status should be still be Pending
      $this->assertEquals($this->membership_status_map["Pending"], $result['status_id']);
      // Dates should be unchanged
      foreach (['start_date', 'end_date', 'join_date'] as $date) {
        $this->assertEquals($membership[$date] ?? NULL, $result[$date] ?? NULL);
      }
    }
  }

  public function dataForTestCompleteRedirectFlowCiviCore() {
    return [
      [ 0, FALSE ],
      [ 7, FALSE ],
      [ 0, TRUE ],
      [ 7, TRUE ]
    ];
  }
  /**
   * Rather heavily overladen DRY code.
   *
   * $overrides may contain keys 'recur', 'order', 'membership' with array
   * values of overrides to the defaults below.
   *
   * If the overrides contains a 'membership' key at all then a membership is included.
   *
   * If $completePayment is TRUE then a payment will be added, which should
   * bump the statuses (Contrib, Membership) accordingly.
   *
   * @return array with the following things in order:
   * - contactID
   * - recur
   * - contrib
   * - membership
   */
  protected function createFixture(?Array $overrides=NULL, ?bool $completePayment=FALSE) {
    $overrides = $overrides ?? [];
    $handlingMembership = (array_key_exists('membership', $overrides));
    foreach ($overrides as $key=>$val) {
      if (!in_array($key, ['order', 'recur', 'membership'])) {
        $this->assertTrue(FALSE, "FIX THIS, got key $key");
      }
    }

    $return = [
      'contactID' => $this->createTestContact(),
      'recur' => [],
      'contrib' => [],
      'membership' => [],
    ];
    $contactID = $return['contactID'];

    $return['recur'] = $this->createContribRecur($overrides['recur'] ?? []);
    // Reload with getsingle for consistency.
    $return['recur'] = civicrm_api3('ContributionRecur', 'getsingle', ['id' => $return['recur']['id']]);

    if ($handlingMembership) {
      // array_filter is used so you can remove default items via override,
      // e.g. passing [status_id] => NULL will remove that.
      $entityParams = array_filter(($overrides['membership'] ?? [] ) + [
        'membership_type_id' => 'MyMembershipType',
        'contact_id' => $contactID,
        'contribution_recur_id' => $return['recur']['id'],
        'join_date' => $this->defaultDate,
        'start_date' => $this->defaultDate,

        // Needed to override default status calculation
        'skipStatusCal' => 1,
        'status_id' => 'Pending',
      ]);
      $lineItemOverrides = [
        // Nb. the Order API seems to look for this being in the line items, but surely it should be in the params?
        // Duplicating it here in case it's needed.
        'membership_type_id' => 'MyMembershipType',
        'entity_table' => 'civicrm_membership',
      ];
    }
    else {
      $lineItemOverrides = $entityParams = [];
    }

    $orderCreateParams = ($overrides['order'] ?? []) + [
        'sequential' => 1,
        // Donation
        'financial_type_id' => 1,
        'total_amount' => 1,
        'source' => 'SOURCE_1',
        'receive_date' => $this->defaultDate,
        'contact_id' => $contactID,
        'contribution_recur_id' => $return['recur']['id'],
        'contribution_status_id' => "Pending",
        'is_test' => 1,
        'line_items' => [
          [
            'params' => $entityParams,
            'line_item' => [ $lineItemOverrides + [
              'line_total' => 1,
              'unit_price' => 1,
              "price_field_id" => 1,
              "price_field_value_id" => 1, // xxx ?
              'financial_type_id' => 1, /* 'Donation' */
              'qty' => 1,
            ]]
          ]
        ],
    ];
    $contrib = civicrm_api3('Order', 'create', $orderCreateParams);
    //print json_encode($orderCreateParams, JSON_PRETTY_PRINT);
    $return['contrib'] = civicrm_api3('Order', 'getsingle', ['id' => $contrib['id']]);
    //print json_encode($return['contrib'], JSON_PRETTY_PRINT);

    // Assert receive_date
    $this->assertEquals($orderCreateParams['receive_date'], substr($return['contrib']['receive_date'], 0, 10),
      "receive_date we got back was not the receive date we passed to order.create");

    if ($handlingMembership) {
      // Fetch the membership
      $return['membership'] = civicrm_api3('Membership', 'getsingle', ['id' => $return['contrib']['line_items'][0]['entity_id']]);
      // Assert start_date
      if (!empty($orderCreateParams['line_items'][0]['params']['start_date'])) {
        $this->assertEquals($orderCreateParams['line_items'][0]['params']['start_date'],
          $return['membership']['start_date'],
          "membership start_date we got back was not the date we passed to order.create's line items params");
      }
    }

    if ($completePayment) {
      // We need to complete the payment.
      civicrm_api3('Payment', 'create', [
        'contribution_id' => $return['contrib']['id'],
        'total_amount' => $return['contrib']['total_amount'],
        'trxn_date' => $return['contrib']['receive_date'],
        'trxn_id' => 'PAYMENT_ID',
        'is_send_contribution_notification' => 0,
      ]);

      // Reload.
      $return['contrib'] = civicrm_api3('Order', 'getsingle', ['id' => $contrib['id']]);
      if ($handlingMembership) {
        $return['membership'] = civicrm_api3('Membership', 'getsingle', ['id' => $return['membership']['id']]);
      }
    }

    return array_values($return);
  }
  /**
   * Mock the GC API for redirect flow testing.
   *
   * It sets it up so:
   *
   * - We expect redirectflow.complete to be called. It will return a stub response.
   *
   * - We expect a subscription.create call for the contact, recur and optionally installments,
   *   and we make this return SUBSCRIPTION_ID and the given start_date
   *
   */
  protected function mockGoCardlessApiForTestPaymentProcessor1($contactID, $installments, $recur, $dateSubscriptionStarts = NULL, $expectedIntervalUnit = 'monthly') {
    $dateSubscriptionStarts = $dateSubscriptionStarts ?? $this->defaultDateSubscriptionStarts;

    // Mock the GC API.
    $redirectFlowsService = $this->createMock(\GoCardlessPro\Services\RedirectFlowsService::class);
    $redirectFlowsService
      ->expects($this->once())
      ->method('complete')
      ->willReturn(json_decode('{"redirect_url":"https://gocardless.com/somewhere","id":"RE1234","links":{"mandate":"MANDATEID"}}'));

    $expectedSubscriptionCreateParams = [
      'amount'        => 100,
      'currency'      => 'GBP',
      'interval'      => 1,
      'name'          => 'test contribution',
      'interval_unit' => $expectedIntervalUnit,
      'links'         => ['mandate' => 'MANDATEID'],
      'metadata'      => ['civicrm' => json_encode(['contactID' => $contactID, 'contributionRecurID' => $recur['id']])],
    ];
    if ($installments) {
      $expectedSubscriptionCreateParams['count'] = $installments;
    }
    $subscriptionService = $this->createMock(\GoCardlessPro\Services\SubscriptionsService::class);
    $subscriptionService
      ->expects($this->once())
      ->method('create')
      ->with(['params' => $expectedSubscriptionCreateParams])
      ->willReturn((object) ['start_date' => $dateSubscriptionStarts, 'id' => "SUBSCRIPTION_ID"]);

    $apiMock = $this->createMock(\GoCardlessPro\Client::class);
    $apiMock->method('redirectFlows')->willReturn($redirectFlowsService);
    $apiMock->method('subscriptions')->willReturn($subscriptionService);
    $this->mockGoCardlessApiForTestPaymentProcessor($apiMock);

  }
  /**
   * Calculates hmac signature and prepares a mock GC API to return the payment details.
   *
   * @return string the calculated signature
   */
  protected function mockGoCardlessApiForWebhook(string $webhookBodyJSON, string $paymentGetJSON, string $paymentID = 'PAYMENT_ID') :string {

    // Mock the GC API.
    $paymentsService = $this->createMock(\GoCardlessPro\Services\PaymentsService::class);
    $paymentsService
      ->expects($this->once())
      ->method('get')
      ->with($paymentID)
      ->willReturn(json_decode($paymentGetJSON));

    $apiMock = $this->createMock(\GoCardlessPro\Client::class);
    $apiMock->method('payments')->willReturn($paymentsService);

    $this->mockGoCardlessApiForTestPaymentProcessor($apiMock);

    $calculatedSignature = hash_hmac("sha256", $webhookBodyJSON, 'mock_webhook_key');
    return $calculatedSignature;
  }
  /**
   * Calculates hmac signature and prepares a mock GC API to return the subscription details.
   *
   * @return string the calculated signature
   */
  protected function mockGoCardlessApiForWebhookSubscription(string $webhookBodyJSON, string $subscriptionGetJSON) :string {

    $subscriptionService = $this->createMock(\GoCardlessPro\Services\SubscriptionsService::class);
    $subscriptionService
      ->expects($this->once())
      ->method('get')
      ->with('SUBSCRIPTION_ID')
      ->willReturn(json_decode($subscriptionGetJSON));

    $apiMock = $this->createMock(\GoCardlessPro\Client::class);
    $apiMock->method('subscriptions')->willReturn($subscriptionService);

    $this->mockGoCardlessApiForTestPaymentProcessor($apiMock);

    $calculatedSignature = hash_hmac("sha256", $webhookBodyJSON, 'mock_webhook_key');
    return $calculatedSignature;
  }
  /**
   * @dataProvider dataForTestRenewingExistingMembership
   */
  public function testRenewingExistingMembership($type) {

    $contactID = $this->createTestContact();

    $dt = new DateTimeImmutable();
    if ($type === 'Grace') {
      // Renewing after the end of a membership.
      $membershipParamsOverride = [
        'join_date'  => $dt->modify("-25 months")->format("Y-m-d"),
        'start_date' => $dt->modify("-13 months")->format("Y-m-d"),
        'end_date'   => $dt->modify("-1 months")->format("Y-m-d"),
      ];
      list($contrib, $membership) = $this->createExistingMembershipNotPaidByGC($contactID, $membershipParamsOverride, $type);
      // Force the status to grace with this hack. (I don't know why the dates don't cause it to be grace already)
      civicrm_api3('Membership', 'create', [
        'id' => $membership['id'],
        'status_id' => 'Grace',
        'skipStatusCal' => 1,
      ]);
      $membership = civicrm_api3('Membership', 'getsingle', ['id' => $membership['id']]);
      $this->assertMembershipStatus($membership, 'Grace');
    }
    else {
      // Renewing a membership that is current (New).
      $membershipParamsOverride = [
        'join_date'  => $dt->modify("-1 months")->format("Y-m-d"),
        'start_date' => $dt->modify("-1 months")->format("Y-m-d"),
        'end_date'   => $dt->modify("-1 months + 1 year - 1 day")->format("Y-m-d"),
      ];
      list($contrib, $membership) = $this->createExistingMembershipNotPaidByGC($contactID, $membershipParamsOverride, $type);
      $this->assertMembershipStatus($membership, 'New');
    }

    // ---------------------------
    // Now we're simulating a contribution page used to renew the membership using GC.
    $origMembership = $membership;

    $dateFormSubmitted = '2021-01-01';
    $fixtureOverrides = [
      'recur' => ['start_date'   => $dateFormSubmitted],
      'order' => ['receive_date' => $dateFormSubmitted],
      // Point to existing membership by providing the 'id' field in the entity params.
      // Ensure we are not setting dates or statuses (the NULLs are removed by the createFixture call)
      'membership' => [
        'id'         => $membership['id'],
        'status_id'  => NULL,
        'join_date'  => NULL,
        'start_date' => NULL,
        'end_date'   => NULL,
      ],
    ];
    list($contactID, $recur, $contrib, $membership) = $this->createFixture($fixtureOverrides, FALSE);
    $this->assertEquals($origMembership['id'], $membership['id'], 'Expected membership ID to be the same, but looks like a new membership was created.');

    // Hack to workaround Core bugs in 5.40 {{{
    if ($this->bugs['orderCreateSetsMembershipPending']['applies']) {
      // Manually update the membership to original status.
      civicrm_api3('Membership', 'create', [
        'id' => $origMembership['id'],
        'status_id' => $origMembership['status_id'],
        'skipStatusCal' => 1,
      ]);
      // Reload membership
      $membership = civicrm_api3('Membership', 'getsingle', ['id' => $origMembership['id']]);
    }
    // }}}

    // Membership status should be still be as it was
    $this->assertMembershipStatus($membership, $type);

    // Mock the GC API to return a redirect flow ID and a URL.
    $this->mockGoCardlessApiForTestPaymentProcessor1($contactID, 0, $recur, $membershipParamsOverride['start_date']);

    // ---------------
    // Call the thing we want to test
    //
    // Params are usually assembled by the civicrm_buildForm hook.
    $params = [
      'test_mode' => TRUE,
      'redirect_flow_id' => 'RE1234',
      'session_token' => 'aabbccdd',
      'description' => 'test contribution',
      'contactID' => $contactID,
      'contributionID' => $contrib['id'],
      'contributionRecurID' => $recur['id'],
      'membershipID' => $membership['id'],
      'payment_processor_id' => $this->test_mode_payment_processor['id'],
      'entryURL' => 'http://example.com/somewhere',
    ];
    CRM_GoCardlessUtils::completeRedirectFlowCiviCore($params);

    // -------------
    // Test it
    $membershipUpdated = civicrm_api3('Membership', 'getsingle', ['id' => $membership['id']]);

    // Dates should be unchanged
    foreach (['start_date', 'end_date', 'join_date'] as $date) {
      $this->assertEquals($membership[$date], $membershipUpdated[$date]);
    }

    if ($this->bugs['orderCreateSetsMembershipPending']['applies']) {
      $this->markTestIncomplete("Tests passed until checking membership dates. Skipping that because of core bug. "
        .$this->bugs['orderCreateSetsMembershipPending']['description']
      );
    }
    else {
      // Membership status should be still be what it was
      $this->assertMembershipStatus($membershipUpdated, $type);
    }
  }
  public function dataForTestRenewingExistingMembership() {
    return [ ['New'], ['Grace'] ];
  }
  /**
   * This is similar to testTransferCheckoutCompletesWithoutInstallments except
   * that it tests that handleRedirectFlowWithGoCardless can be overridden by a
   * hook.
   *
   * This creates a contact with a contribution and a ContributionRecur in the
   * same way that CiviCRM's core Contribution Pages form does, then, having
   * mocked the GC API it calls
   * CRM_GoCardlessUtils::completeRedirectFlowCiviCore()
   * and checks that the result is updated contribution and ContributionRecur records.
   *
   * testing with no membership
   */
  public function testHookHandleRedirectFlowWithGoCardless() {
    $fixtureOverrides = [];
    list($contactID, $recur, $contrib, $membership) = $this->createFixture($fixtureOverrides, FALSE);
    // Sanity checks.
    $this->assertContributionRecurStatus($recur, 'Pending');
    $this->assertArrayNotHasKey('processor_id', $recur);

    // Prepare our test hook.
    $this->handleRedirectFlowHookTest = [$this, 'hookHandleRedirectFlowWithGoCardless'];

    // Params are usually assembled by the civicrm_buildForm hook.
    $params = [
      'test_mode' => TRUE,
      'redirect_flow_id' => 'RE1234',
      'session_token' => 'aabbccdd',
      'description' => 'test contribution',
      'contactID' => $contactID,
      'contributionID' => $contrib['id'],
      'contributionRecurID' => $recur['id'],
      'payment_processor_id' => $this->test_mode_payment_processor['id'],
      'entryURL' => 'http://example.com/somwhere',
    ];
    CRM_GoCardlessUtils::completeRedirectFlowCiviCore($params);

    // Check our hook got called.
    $this->assertTrue($this->hookWasCalled, "Expected the hook to be called, but it wasn't.");

    // Now test the contributions were NOT updated (our hook completely bypasses normal GC stuff)
    $recurNow = civicrm_api3('ContributionRecur', 'getsingle', ['id' => $recur['id']]);
    $this->assertEquals($recur, $recurNow);

    // Check the contrib hasn't changed.
    $contribNow = civicrm_api3('Order', 'getsingle', ['id' => $contrib['id']]);
    $this->assertEquals($contrib, $contribNow);

  }
  /**
   * An implementation of hook_civicrm_handleRedirectFlowWithGoCardless
   *
   * Used by testHookHandleRedirectFlowWithGoCardless
   */
  public function hookHandleRedirectFlowWithGoCardless($details, &$result) {
    $this->hookWasCalled = TRUE;
    $this->assertIsArray($details);
    $result['subscription'] = 'SUBSCRIPTION_ID_HOOKED';
  }

  /**
   * Check missing signature throws InvalidArgumentException.
   */
  public function testWebhookMissingSignature() {
    $this->expectException(InvalidArgumentException::class);
    $this->expectExceptionMessage("Unsigned API request.");
    $controller = new CRM_Core_Payment_GoCardlessIPN();
    $controller->parseWebhookRequest([], '');
  }

  /**
   * Check wrong signature throws InvalidArgumentException.
   *
   */
  public function testWebhookWrongSignature() {
    $this->expectException(InvalidArgumentException::class);
    $this->expectExceptionMessage("Invalid signature in request.");
    $controller = new CRM_Core_Payment_GoCardlessIPN();
    $controller->parseWebhookRequest(["Webhook-Signature" => 'foo'], 'bar');
  }

  /**
   * Check events extracted from webhook.
   *
   */
  public function testWebhookParse() {
    $controller = new CRM_Core_Payment_GoCardlessIPN();
    $body = '{"events":[
      {"id":"EV1","resource_type":"payments","action":"confirmed"},
      {"id":"EV2","resource_type":"payments","action":"failed"},
      {"id":"EV3","resource_type":"payments","action":"something we do not handle"},
      {"id":"EV4","resource_type":"subscriptions","action":"cancelled"},
      {"id":"EV5","resource_type":"subscriptions","action":"finished"},
      {"id":"EV6","resource_type":"subscriptions","action":"something we do not handle"},
      {"id":"EV7","resource_type":"unhandled_resource","action":"foo"}
      ]}';
    $calculated_signature = hash_hmac("sha256", $body, 'mock_webhook_key');
    $controller->parseWebhookRequest(["Webhook-Signature" => $calculated_signature], $body);

    $this->assertIsArray($controller->events);
    foreach (['EV1', 'EV2', 'EV4', 'EV5'] as $event_id) {
      $this->assertArrayHasKey($event_id, $controller->events);
    }
    $this->assertCount(4, $controller->events);
  }

  /**
   * A payment confirmation should update the initial Pending Contribution.
   *
   * @dataProvider withOrWithoutTemplate
   */
  public function testWebhookPaymentConfirmationFirst(bool $withTemplateContribution) {
    // when webhook called
    $dt = new DateTimeImmutable();
    $today = $dt->format("Y-m-d");
    $charge_date = $dt->modify("-2 days")->format("Y-m-d");

    $fixtureOverrides = [
      'recur' => [
        'start_date' => $charge_date,
        'processor_id' => 'SUBSCRIPTION_ID', /* simulate a complete redirect flow */
      ],
      'order' => [ 'receive_date' => $charge_date ],
      'membership' => [
        'status_id' => NULL,
        'start_date' => NULL,
        'end_date' => NULL,
        'join_date' => NULL,
      ],
    ];
    list($contactID, $recur, $contrib, $membership) = $this->createFixture($fixtureOverrides, FALSE);

    if ($withTemplateContribution) {
      CRM_Contribute_BAO_ContributionRecur::ensureTemplateContributionExists($recur['id']);
    }

    // Mock webhook input data.
    $body = '{"events":[
      {"id":"EV1","resource_type":"payments","action":"confirmed","links":{"payment":"PAYMENT_ID"}}
      ]}';
    $calculatedSignature = $this->mockGoCardlessApiForWebhook($body, '{
        "id":"PAYMENT_ID",
        "status":"confirmed",
        "charge_date":"' . $charge_date . '",
        "amount":5000,
        "links":{"subscription":"SUBSCRIPTION_ID"}
      }');

    // Now trigger webhook.
    $controller = new CRM_Core_Payment_GoCardlessIPN();
    $controller->parseWebhookRequest(["Webhook-Signature" => $calculatedSignature], $body);
    $controller->processWebhookEvents(TRUE);

    // Now check the changes have been made.
    $contrib = civicrm_api3('Contribution', 'getsingle', ['id' => $contrib['id']]);
    $this->assertEquals($charge_date . ' 00:00:00', $contrib['receive_date']);
    $this->assertEquals('PAYMENT_ID', $contrib['trxn_id']);
    $this->assertContributionStatus($contrib, 'Completed');
    // Nb. this is an edge case:
    $this->assertEquals(50, $contrib['total_amount']);

    $membershipUpdated = civicrm_api3('Membership', 'getsingle', ['id' => $membership['id']]);
    // status should be updated to New
    $this->assertEquals($this->membership_status_map["New"], $membershipUpdated['status_id'], "Wanted status New, got status " . CRM_Member_PseudoConstant::membershipstatus()[$membership['status_id']]);
    // join_date should be unchanged
    $this->assertEquals($membership['join_date'] ?? NULL, $membershipUpdated['join_date'] ?? NULL);

    // start_date set to setup date (i.e. probably? unchanged)
    // The original test expected this to be today's date, but while testing with 5.19.1
    // this was not the case. As those changes happen outside of this payment processor
    // I decided to go with what core now does...
    // 5.40: now it seems to be today's date. This assertion is only here so I can track
    // changes in what core does, so I have updated the assertion to today.
    $this->assertEquals($today, $membershipUpdated['start_date']);
    // end_date updated
    $this->assertEquals((new DateTimeImmutable($today))->modify("+1 year")->modify("-1 day")->format("Y-m-d"), $membershipUpdated['end_date']);

    // Check the recur record has been updated.
    $recur = civicrm_api3('ContributionRecur', 'getsingle', ['id' => $recur['id']]);
    $this->assertContributionRecurStatus($recur, 'In Progress');
  }

  public function withOrWithoutTemplate() {
    return [
      'with template' => [TRUE],
      'without template' => [FALSE],
    ];
  }
  /**
   * A payment confirmation should create a new contribution.
   *
   * Fixture:
   * - Create a recurring payment with 1 *completed* payment 1 year ago
   *
   * @dataProvider withOrWithoutTemplate
   */
  public function testWebhookPaymentConfirmationSubsequent(bool $withTemplateContribution) {
    // when webhook called
    $dt = new DateTimeImmutable();
    $first_date_string = $dt->modify('-1 year')->format('Y-m-d');
    $second_charge_date = $dt->format('Y-m-d');

    list($contactID, $recur, $contrib, $membership) = $this->createFixture([
      'recur' => [
        'contribution_status_id' => "In Progress",
        'start_date' => $first_date_string,
        'processor_id' => 'SUBSCRIPTION_ID',
      ],
      'order' => [
        'receive_date' => $first_date_string,
      ],
      'membership' => [
        'start_date' => $first_date_string,
        'join_date' => $first_date_string,
      ]
    ], TRUE);

    // Hack to workaround Core bugs in 5.40 {{{
    if ($this->bugs['coreIssue2791']['applies']) {
      // Manually update the membership date.
      civicrm_api3('Membership', 'create', [
        'id' => $membership['id'],
        'start_date' => $first_date_string,
        'end_date' => $dt->modify('-1 day')->format('Y-m-d'),
        'skipStatusCal' => 1,
      ]);
      // Reload membership
      $membership = civicrm_api3('Membership', 'getsingle', ['id' => $membership['id']]);
    }
    // }}}

    // Check membership start_date
    $this->assertEquals($first_date_string, $membership['start_date'],
      "The membership start_date is not what we expected; not the date we passed in.");
    // Check end date is as expected.
    $this->assertEquals((new DateTimeImmutable($first_date_string))->modify("+1 year")->modify("-1 day")->format("Y-m-d"), $membership['end_date'],
      "The membership end_date is not as expected"
    );

    // Mock webhook input data.
    $body = '{"events":[
      {"id":"EV1","resource_type":"payments","action":"confirmed","links":{"payment":"PAYMENT_ID_2"}}
      ]}';
    $calculatedSignature = $this->mockGoCardlessApiForWebhook($body, '{
        "id":"PAYMENT_ID_2",
        "status":"confirmed",
        "charge_date": "' . $second_charge_date . '",
        "amount":123,
        "links":{"subscription":"SUBSCRIPTION_ID"}
        }', 'PAYMENT_ID_2');

    if ($withTemplateContribution) {
      CRM_Contribute_BAO_ContributionRecur::ensureTemplateContributionExists($recur['id']);
    }


    //
    // Mocks, done, now onto the code we are testing: trigger webhook.
    //
    $controller = new CRM_Core_Payment_GoCardlessIPN();
    $controller->parseWebhookRequest(["Webhook-Signature" => $calculatedSignature], $body);
    $controller->processWebhookEvents(TRUE);

    //
    // Now check the changes have been made.
    //
    $result = civicrm_api3('Contribution', 'get', [
      'contribution_recur_id' => $recur['id'],
      'is_test' => 1,
    ]);
    // Should be 2 records now.
    $this->assertEquals(2, $result['count']);
    // Ensure we have the first one.
    $this->assertArrayHasKey($contrib['id'], $result['values']);
    // Now we can get rid of it.
    unset($result['values'][$contrib['id']]);
    // And the remaining one should be our new one.
    $contrib = reset($result['values']);

    $this->assertEquals("$second_charge_date 00:00:00", $contrib['receive_date']);
    $this->assertEquals(1.23, $contrib['total_amount']);
    $this->assertEquals('PAYMENT_ID_2', $contrib['trxn_id']);
    $this->assertArrayHasKey('contribution_source', $contrib, 'Expected to find `contribution_source` in subsequent contribution, but didn’t.');
    $this->assertEquals('SOURCE_1', $contrib['contribution_source']);
    $this->assertEquals($this->contribution_status_map['Completed'], $contrib['contribution_status_id']);

    // Check membership
    $result = civicrm_api3('Membership', 'getsingle', ['id' => $membership['id']]);
    $this->assertEquals($this->membership_status_map["Current"], $result['status_id']);
    // join_date and start_date are unchanged
    $this->assertEquals($membership['join_date'], $result['join_date']);
    $this->assertEquals($membership['start_date'], $result['start_date']);
    // end_date is 12 months later
    $end_dt = new DateTimeImmutable($membership['end_date']);
    $this->assertEquals($end_dt->modify("+12 months")->format("Y-m-d"), $result['end_date']);


    if ($this->bugs['coreIssue2791']['applies']) {
      $this->markTestIncomplete('Tests passed but using a hack to workaround bug: '
        . $this->bugs['coreIssue2791']['description']
      );
    }
  }

  /**
   * A payment confirmation should not change a recur status from Cancelled to In Progress.
   * See Issue 54
   *
   */
  public function testWebhookPaymentConfirmationDoesNotMarkCancelledAsInProgress() {

    // First, create a contact who has had a membership with one completed
    // payment, that is now cancelled (meaning: the ContributionRecur is cancelled).

    $dt = new DateTimeImmutable();
    $first_date_string = $dt->modify('-1 year')->format('Y-m-d');
    $second_charge_date = $dt->format('Y-m-d');
    list($contactID, $recur, $contrib, $membership) = $this->createFixture([
      'recur' => [
        'start_date' => $first_date_string,
        'contribution_status_id' => "Cancelled", /* Look! */
        'processor_id' => 'SUBSCRIPTION_ID',
      ],
      'order' => [
        'receive_date' => $first_date_string,
      ],
      'membership' => [
        'start_date' => $dt->modify("-11 months")->format("Y-m-d"),
        'join_date' => $dt->modify("-23 months")->format("Y-m-d"),
      ]
    ], TRUE /* complete the contribution */);
    $this->assertContributionRecurStatus($recur, 'Cancelled');

    // Force membership onto Current, then reload it.
    civicrm_api3('Membership', 'create', [
      'id' => $membership['id'],
      'status_id' => "Current",
      'skipStatusCal' => 1,
    ]);
    $membership = civicrm_api3('Membership', 'getsingle', ['id' => $membership['id']]);

    // Mock webhook input data.
    $body = '{"events":[
      {"id":"EV1","resource_type":"payments","action":"confirmed","links":{"payment":"PAYMENT_ID_2"}}
      ]}';
    $calculatedSignature = $this->mockGoCardlessApiForWebhook($body, '{
        "id":"PAYMENT_ID_2",
        "status":"confirmed",
        "charge_date": "' . $second_charge_date . '",
        "amount":123,
        "links":{"subscription":"SUBSCRIPTION_ID"}
        }', 'PAYMENT_ID_2');

    //
    // Mocks, done, now onto the code we are testing: trigger webhook.
    //
    $controller = new CRM_Core_Payment_GoCardlessIPN();
    $controller->parseWebhookRequest(["Webhook-Signature" => $calculatedSignature], $body);
    $controller->processWebhookEvents(TRUE);

    //
    // Now check the changes have been made.
    //
    $result = civicrm_api3('Contribution', 'get', [
      'contribution_recur_id' => $recur['id'],
      'is_test' => 1,
    ]);
    // Should be 2 records now.
    $this->assertEquals(2, $result['count']);
    // Ensure we have the first one.
    $this->assertArrayHasKey($contrib['id'], $result['values']);
    // Now we can get rid of it.
    unset($result['values'][$contrib['id']]);
    // And the remaining one should be our new one.
    $contrib = reset($result['values']);

    // Check the recur status is still cancelled.
    $result = civicrm_api3('ContributionRecur', 'getsingle', ['id' => $contrib['contribution_recur_id']]);
    $contrib_recur_statuses = array_flip($this->contribution_recur_status_map);
    $this->assertEquals($contrib_recur_statuses[$result['contribution_status_id']], 'Cancelled',
      'Expected the contrib recur record to STILL have status Cancelled after a successful contribution received.');
  }

  /**
   * A payment failed should update the initial Pending Contribution.
   *
   * Also, if a successful payment comes in at a later date, that should work normally.
   */
  public function testWebhookPaymentFailedFirst() {

    $fixtureOverrides = [
      'recur' => [
        'processor_id' => 'SUBSCRIPTION_ID', /* simulate a complete redirect flow */
      ],
      'order' => [],
    ];
    list($contactID, $recur, $contrib, $membership) = $this->createFixture($fixtureOverrides, FALSE);

    // Mock webhook input data.
    $body = '{"events":[
      {"id":"EV1","resource_type":"payments","action":"failed","links":{"payment":"PAYMENT_ID"}}
      ]}';
    $calculatedSignature = $this->mockGoCardlessApiForWebhook($body, '{
        "id":"PAYMENT_ID",
          "status":"failed",
          "charge_date":"2016-10-02",
          "amount":123,
          "links":{"subscription":"SUBSCRIPTION_ID"}
        }'
    );

    // -------------------------------------------------------
    // What we're testing: Trigger webhook.
    // -------------------------------------------------------
    $controller = new CRM_Core_Payment_GoCardlessIPN();
    $controller->parseWebhookRequest(["Webhook-Signature" => $calculatedSignature], $body);
    $controller->processWebhookEvents(TRUE);

    // -------------------------------------------------------
    // Test: the initial contrib should show as failed.
    $contrib = civicrm_api3('Contribution', 'getsingle', ['id' => $contrib['id']]);
    $this->assertEquals('2016-10-02 00:00:00', $contrib['receive_date']);
    $this->assertEquals(1.23, $contrib['total_amount']);
    $this->assertEquals('PAYMENT_ID', $contrib['trxn_id']);
    $this->assertContributionStatus($contrib, 'Failed');

    // Test: the contrib recur should be 'overdue'
    $recur = civicrm_api3('ContributionRecur', 'getsingle', ['id' => $recur['id']]);
    $this->assertContributionRecurStatus($recur, 'Overdue');

    // -------------------------------------------------------
    // Mock a second, failed contribution.
    // -------------------------------------------------------
    $body = '{"events":[
      {"id":"EV2","resource_type":"payments","action":"failed","links":{"payment":"PAYMENT_ID_2"}}
      ]}';
    $calculatedSignature = $this->mockGoCardlessApiForWebhook($body, '{
        "id":"PAYMENT_ID_2",
          "status":"failed",
          "charge_date":"2016-11-02",
          "amount":123,
          "links":{"subscription":"SUBSCRIPTION_ID"}
        }', 'PAYMENT_ID_2');

    // -------------------------------------------------------
    // What we're testing: Trigger webhook.
    // -------------------------------------------------------
    $controller = new CRM_Core_Payment_GoCardlessIPN();
    $controller->parseWebhookRequest(["Webhook-Signature" => $calculatedSignature], $body);
    $controller->processWebhookEvents(TRUE);

    // -------------------------------------------------------
    // Test: Original (now failed) payment is unaffected
    $contrib = civicrm_api3('Contribution', 'getsingle', ['id' => $contrib['id']]);
    $this->assertEquals('2016-10-02 00:00:00', $contrib['receive_date']);
    $this->assertEquals(1.23, $contrib['total_amount']);
    $this->assertEquals('PAYMENT_ID', $contrib['trxn_id']);
    $this->assertContributionStatus($contrib, 'Failed');

    // -------------------------------------------------------
    // Test: Should be a new, failed payment
    $contrib = $this->getLatestContribution($recur['id']);
    $this->assertEquals('2016-11-02 00:00:00', $contrib['receive_date']);
    $this->assertEquals(1.23, $contrib['total_amount']);
    $this->assertEquals('PAYMENT_ID_2', $contrib['trxn_id']);
    $this->assertContributionStatus($contrib, 'Failed');


    // -------------------------------------------------------
    // Mock a third, and our first successful ('confirmed'), contribution.
    // @see https://lab.civicrm.org/extensions/gocardless/-/issues/82
    // -------------------------------------------------------
    $body = '{"events":[
      {"id":"EV3","resource_type":"payments","action":"confirmed","links":{"payment":"PAYMENT_ID_3"}}
      ]}';
    $calculatedSignature = $this->mockGoCardlessApiForWebhook($body, '{
        "id":"PAYMENT_ID_3",
        "status":"confirmed",
        "charge_date":"2016-12-02",
        "amount":123,
        "links":{"subscription":"SUBSCRIPTION_ID"}
      }', 'PAYMENT_ID_3');

    // -------------------------------------------------------
    // What we're testing: Trigger webhook.
    // -------------------------------------------------------
    $controller = new CRM_Core_Payment_GoCardlessIPN();
    $controller->parseWebhookRequest(["Webhook-Signature" => $calculatedSignature], $body);
    $controller->processWebhookEvents(TRUE);

    // -------------------------------------------------------
    // Test: Should be a new, successful payment
    $contrib = $this->getLatestContribution($recur['id']);
    $this->assertEquals('2016-12-02 00:00:00', $contrib['receive_date']);
    $this->assertEquals(1.23, $contrib['total_amount']);
    $this->assertEquals('PAYMENT_ID_3', $contrib['trxn_id']);
    $this->assertContributionStatus($contrib, 'Completed');

    // -------------------------------------------------------
    // Test: the contrib recur should be In Progress again.
    $recur = civicrm_api3('ContributionRecur', 'getsingle', ['id' => $recur['id']]);
    $this->assertContributionRecurStatus($recur, 'In Progress');

  }

  /**
   * A payment confirmation should create a new contribution.
   *
   * @dataProvider withOrWithoutTemplate
   */
  public function testWebhookPaymentFailedSubsequent($withTemplateContribution) {

    // Fixture: completed redirect flow with one payment complete
    $fixtureOverrides = [
      'recur' => ['processor_id' => 'SUBSCRIPTION_ID']
    ];
    list($contactID, $recur, $contrib, $membership) = $this->createFixture($fixtureOverrides, TRUE);
    if ($withTemplateContribution) CRM_Contribute_BAO_ContributionRecur::ensureTemplateContributionExists($recur['id']);

    // Mock webhook input data.
    $body = '{"events":[
      {"id":"EV1","resource_type":"payments","action":"failed","links":{"payment":"PAYMENT_ID_2"}}
      ]}';
    $calculatedSignature = $this->mockGoCardlessApiForWebhook($body, '{
        "id":"PAYMENT_ID_2",
        "status":"failed",
        "charge_date":"2016-10-02",
        "amount":123,
        "links":{"subscription":"SUBSCRIPTION_ID"}
      }', 'PAYMENT_ID_2');

    // Now trigger webhook.
    $controller = new CRM_Core_Payment_GoCardlessIPN();
    $controller->parseWebhookRequest(["Webhook-Signature" => $calculatedSignature], $body);
    $controller->processWebhookEvents(TRUE);

    // Now check the changes have been made.
    $result = civicrm_api3('Contribution', 'get', [
      'contribution_recur_id' => $recur['id'],
      'is_test' => 1,
    ]);
    // Should be 2 records now.
    $this->assertEquals(2, $result['count']);
    // Ensure we have the first one.
    $this->assertArrayHasKey($contrib['id'], $result['values']);
    // Now we can get rid of it.
    unset($result['values'][$contrib['id']]);
    // And the remaining one should be our new one.
    $contrib = reset($result['values']);

    $this->assertEquals('2016-10-02 00:00:00', $contrib['receive_date']);
    $this->assertEquals(1.23, $contrib['total_amount']);
    $this->assertEquals('PAYMENT_ID_2', $contrib['trxn_id']);
    $this->assertContributionStatus($contrib, 'Failed');

  }

  /**
   * Late Payments.
   * See Issue 55
   */
  public function testWebhookPaymentFailedLate() {

    // Fixture: completed redirect flow with one payment complete
    $fixtureOverrides = [
      'recur' => ['processor_id' => 'SUBSCRIPTION_ID']
    ];
    list($contactID, $recur, $contrib, $membership) = $this->createFixture($fixtureOverrides, TRUE);

    // Now force the CR to cancelled.
    $recur = civicrm_api3('ContributionRecur', 'create', array(
      'id' => $recur['id'],
      'contribution_status_id' => "Cancelled",
    ));

    // Mock webhook input data.
    $body = '{"events":[
      {"id":"EV1","resource_type":"payments","action":"failed","links":{"payment":"PAYMENT_ID"}}
      ]}';
    $calculatedSignature = $this->mockGoCardlessApiForWebhook($body, '{
        "id":"PAYMENT_ID",
        "status":"failed",
        "charge_date":"' . $this->defaultDateSubscriptionStarts . '",
        "amount":123,
        "links":{"subscription":"SUBSCRIPTION_ID"}
        }');

    // Now trigger webhook.
    $controller = new CRM_Core_Payment_GoCardlessIPN();
    $controller->parseWebhookRequest(["Webhook-Signature" => $calculatedSignature], $body);
    $controller->processWebhookEvents(TRUE);

    // Now check the changes have been made.
    $contrib = civicrm_api3('Contribution', 'getsingle', ['id' => $contrib['id']]);
    $this->assertContributionStatus($contrib, 'Refunded');
    $this->assertEquals('PAYMENT_ID', $contrib['trxn_id']);
  }

  /**
   * A payment confirmation webhook that is out of date.
   *
   */
  public function testWebhookOutOfDate() {

    $this->expectException(CRM_GoCardless_WebhookEventIgnoredException::class);
    $this->expectExceptionMessage("OK: Webhook out of date, expected status confirmed, got 'cancelled'");

    $this->mockGoCardlessApiForWebhook('', '{
        "id":"PAYMENT_ID",
        "status":"cancelled",
        "charge_date":"2016-10-02",
        "amount":123,
        "links":{"subscription":"SUBSCRIPTION_ID"}
      }');

    // Now trigger webhook.
    $event = json_decode(json_encode(['links' => ['payment' => 'PAYMENT_ID']]));
    // Calling with different status to that which will be fetched from API.
    $controller = $this->getWebhookControllerForTestProcessor();
    $controller->getAndCheckGoCardlessPayment($event, ['confirmed']);
  }

  /**
   * A subscription cancelled webhook that is out of date.
   *
   */
  public function testWebhookOutOfDateSubscription() {

    $this->expectException(CRM_GoCardless_WebhookEventIgnoredException::class);
    $this->expectExceptionMessage("OK: Skipping this event as it is out of date, expected subscription status 'complete', got 'cancelled'");

    $this->mockGoCardlessApiForWebhookSubscription('',
     '{"status": "cancelled", "id": "SUBSCRIPTION_ID"}'
    );

    $event = json_decode('{"links":{"subscription":"SUBSCRIPTION_ID"}}');
    $controller = $this->getWebhookControllerForTestProcessor();
    // Calling with different status to that which will be fetched from API.
    $controller->getAndCheckSubscription($event, 'complete');
  }

  /**
   * A payment confirmation webhook event that does not have a subscription
   * should be ignored.
   *
   * Note that coverage wise, this test also tests that exceptions are handled in the processWebhookEvents, processWebhookEvent methods.
   */
  public function testWebhookPaymentWithoutSubscriptionIgnored() {

    $this->mockGoCardlessApiForWebhook('', '{
        "id":"PAYMENT_ID",
          "status":"confirmed",
          "charge_date":"2016-10-02",
          "amount":123,
          "links":{}
        }');

    // Now trigger webhook.
    $event = json_decode(json_encode(['id' => 'EV1', 'links' => ['payment' => 'PAYMENT_ID'], 'resource_type' => 'payments', 'action' => 'confirmed']));
    // Calling with different status to that which will be fetched from API.
    $controller = $this->getWebhookControllerForTestProcessor();
    $controller->events = [$event];
    // Test the inner method
    $result = $controller->processWebhookEvent($event);
    $this->assertEquals('OK: Ignored payment that does not belong to a subscription (all CiviCRM-related ones would belong to a subscription).', $result->message);
    $this->assertEquals(TRUE, $result->ok);
    $this->assertInstanceOf(\CRM_GoCardless_WebhookEventIgnoredException::class, $result->exception);
    // Test the loop method, no throw (execution should continue)
    $this->mockGoCardlessApiForWebhook('', '{
        "id":"PAYMENT_ID",
          "status":"confirmed",
          "charge_date":"2016-10-02",
          "amount":123,
          "links":{}
        }');
    $controller->processWebhookEvents(FALSE);
    $this->assertTrue(TRUE);
    // Test the throw way
    $this->mockGoCardlessApiForWebhook('', '{
        "id":"PAYMENT_ID",
          "status":"confirmed",
          "charge_date":"2016-10-02",
          "amount":123,
          "links":{}
        }');
    $this->expectException(CRM_GoCardless_WebhookEventIgnoredException::class);
    $this->expectExceptionMessage("OK: Ignored payment that does not belong to a subscription (all CiviCRM-related ones would belong to a subscription).");
    $controller->processWebhookEvents(TRUE);
  }

  /**
   * A subscription cancelled should update the recurring contribution record
   * and a Pending Contribution.
   *
   * @dataProvider dataForTestWebhookSubscriptionEnded
   *
   */
  public function testWebhookSubscriptionEnded($event, $expectedStatus) {

    $finishDate = date('Y-m-d', strtotime($this->defaultDateSubscriptionStarts . ' + 3 month'));

    $fixtureOverrides = [
      'recur' => [
        'processor_id' => 'SUBSCRIPTION_ID', /* simulate a complete redirect flow */
      ],
      'order' => [],
    ];
    list($contactID, $recur, $contrib, $membership) = $this->createFixture($fixtureOverrides, FALSE);

    // Mock webhook input data.
    $body = '{"events":[
      {"id":"EV1","resource_type":"subscriptions","action":"' .$event . '","links":{"subscription":"SUBSCRIPTION_ID"}}
      ]}';
    $calculatedSignature = $this->mockGoCardlessApiForWebhookSubscription($body, '{
        "id":"SUBSCRIPTION_ID",
        "status":"' . $event . '",
        "end_date":"' . $finishDate . '"
      }');

    // Now trigger webhook.
    $controller = new CRM_Core_Payment_GoCardlessIPN();
    $controller->parseWebhookRequest(["Webhook-Signature" => $calculatedSignature], $body);
    $controller->processWebhookEvents(TRUE);

    // Now check the changes have been made to the original contribution.
    $contrib = civicrm_api3('Contribution', 'getsingle', [
      'contribution_recur_id' => $recur['id'],
      'is_test' => 1,
    ]);
    $this->assertContributionStatus($contrib, 'Cancelled');

    // Now check the changes have been made to the recurring contribution.
    $recur = civicrm_api3('ContributionRecur', 'getsingle', ['id' => $recur['id']]);
    $this->assertEquals("$finishDate 00:00:00", $recur['end_date']);
    $this->assertContributionRecurStatus($recur, $expectedStatus);
  }

  public function dataForTestWebhookSubscriptionEnded() {
    return [
      ['cancelled', 'Cancelled'],
      ['finished', 'Completed'],
    ];
  }
  /**
   * A payment confirmation should update the initial Pending Contribution.
   *
   * This is the same test as testWebhookPaymentConfirmationFirst except that it
   * uses the queue.
   */
  public function testWebhookQueueSupport() {
    // when webhook called
    $dt = new DateTimeImmutable();
    $today = $dt->format("Y-m-d");
    $charge_date = $dt->modify("-2 days")->format("Y-m-d");

    $fixtureOverrides = [
      'recur' => [
        'start_date' => $charge_date,
        'processor_id' => 'SUBSCRIPTION_ID', /* simulate a complete redirect flow */
      ],
      'order' => [ 'receive_date' => $charge_date ],
      'membership' => [
        'status_id' => NULL,
        'start_date' => NULL,
        'end_date' => NULL,
        'join_date' => NULL,
      ],
    ];
    list($contactID, $recur, $contrib, $membership) = $this->createFixture($fixtureOverrides, FALSE);

    // Mock webhook input data.
    $body = '{"meta": {"webhook_id": "WEBHOOK_ID"}, "events":[
      {"id":"EV1","resource_type":"payments","action":"confirmed","links":{"payment":"PAYMENT_ID"}}
      ]}';
    $calculatedSignature = $this->mockGoCardlessApiForWebhook($body, '{
        "id":"PAYMENT_ID",
        "status":"confirmed",
        "charge_date":"' . $charge_date . '",
        "amount":5000,
        "links":{"subscription":"SUBSCRIPTION_ID"}
      }');

    // Now trigger webhook via handleRequest().
    $controller = $this->getWebhookControllerForTestProcessor();
    $result = $controller->handleRequest(["Webhook-Signature" => $calculatedSignature], $body);
    $this->assertEquals(204, $result);

    // Load the queue and inspect.
    $queueItems = PaymentprocessorWebhook::get(FALSE)->execute()->getArrayCopy();
    $this->assertEquals(1, count($queueItems), "Expected that the event was queued as PaymentprocessorWebhook");

    // Re-send the webhook so we have a duplicate.
    $controller = $this->getWebhookControllerForTestProcessor();
    $result = $controller->handleRequest(["Webhook-Signature" => $calculatedSignature], $body);
    $this->assertEquals(204, $result);

    // Now run the queue
    $result = civicrm_api3('Job', 'process_paymentprocessor_webhooks', []);

    // Load the queue and inspect.
    $queueItems = PaymentprocessorWebhook::get(FALSE)->addOrderBy('id')->execute()->getArrayCopy();
    $this->assertEquals(2, count($queueItems), "Expected 2 queue items - the original and the duplicate.");
    $this->assertEquals('success', $queueItems[0]['status'], "Expected first queue item to have succeeded.");
    $this->assertStringStartsWith("OK: PaymentsConfirmed completed. Recur:", $queueItems[0]['message'], "Expected second queue item to have errored.");
    $this->assertEquals('error', $queueItems[1]['status'], "Expected second queue item to have errored.");
    $this->assertEquals('Refusing to process this event as it is a duplicate.', $queueItems[1]['message'], "Expected second queue item to have errored.");

    // Now check the changes have been made.
    $contrib = civicrm_api3('Contribution', 'getsingle', ['id' => $contrib['id']]);
    $this->assertEquals($charge_date . ' 00:00:00', $contrib['receive_date']);
    $this->assertEquals('PAYMENT_ID', $contrib['trxn_id']);
    $this->assertContributionStatus($contrib, 'Completed');
    // Nb. this is an edge case:
    $this->assertEquals(50, $contrib['total_amount']);

    $membershipUpdated = civicrm_api3('Membership', 'getsingle', ['id' => $membership['id']]);
    // status should be updated to New
    $this->assertEquals($this->membership_status_map["New"], $membershipUpdated['status_id'], "Wanted status New, got status " . CRM_Member_PseudoConstant::membershipstatus()[$membership['status_id']]);
    // join_date should be unchanged
    $this->assertEquals($membership['join_date'] ?? NULL, $membershipUpdated['join_date'] ?? NULL);

    // start_date set to setup date (i.e. probably? unchanged)
    // The original test expected this to be today's date, but while testing with 5.19.1
    // this was not the case. As those changes happen outside of this payment processor
    // I decided to go with what core now does...
    // 5.40: now it seems to be today's date. This assertion is only here so I can track
    // changes in what core does, so I have updated the assertion to today.
    $this->assertEquals($today, $membershipUpdated['start_date']);
    // end_date updated
    $this->assertEquals((new DateTimeImmutable($today))->modify("+1 year")->modify("-1 day")->format("Y-m-d"), $membershipUpdated['end_date']);

    // Check the recur record has been updated.
    $recur = civicrm_api3('ContributionRecur', 'getsingle', ['id' => $recur['id']]);
    $this->assertContributionRecurStatus($recur, 'In Progress');
  }

  /**
   * Test some weird cases.
   */
  public function testGetOriginalContribution() {
    /** @var \Civi\GoCardless\Log $logger */
    $logger = Civi::log('GoCardless');

    $this->createTestContact();
    // Test broken recurs.
    // Create a recur with no contributions.
    $recur = \Civi\Api4\ContributionRecur::create(FALSE)
      ->setValues([
        'amount' => 1,
        'financial_type_id' => 1,
        'contact_id' => $this->contactID
      ])->execute()->first();

    $controller = $this->getWebhookControllerForTestProcessor();
    $result = $controller->getOriginalContribution($recur, FALSE);
    $this->assertEquals(['_was' => 'not_found'], $result);
    $this->assertStringContainsString('FAILED: Did not find any Contributions at all for ContributionRecur',
      end($logger->testingLog)
    );

    // Test that getOriginalContribution fails if a recur with one failed contrib exists and we tell it not to create a template.
    // reset log
    $logger->testingLog = [];
    // Create a failed contrib.
    $failedContrib = \Civi\Api4\Contribution::create(FALSE)
      ->setValues([
        'contribution_recur_id' => $recur['id'],
        'contribution_status_id:name' => 'Failed',
        'receive_date' => 'today',
        'total_amount' => 1,
        'financial_type_id' => 1,
        'contact_id' => $this->contactID,
      ])->execute()->first();
    $result = $controller->getOriginalContribution($recur, FALSE);
    $this->assertEquals(['_was' => 'not_found'], $result);
    $this->assertStringContainsString('UNEXPECTED: Did not find any completed or template Contributions to use.',
      end($logger->testingLog)
    );

    // Test that getOriginalContribution returns template.
    $logger->testingLog = [];
    $result = $controller->getOriginalContribution($recur, TRUE);
    $this->assertEquals('found_template', $result['_was']);
    $this->assertNotEquals($failedContrib['id'], $result['id'], "Expect template contribution to have different ID to failed Contribution");
    $allLogs = implode("\n", $logger->testingLog);
    $this->assertStringContainsString('Did not find any completed or template Contributions but found a non-completed Contribution. Trying to create a template now for ContributionRecur', $allLogs);
    $this->assertStringContainsString('Template created', $allLogs);

  }
  public function testGetContributionRecurFromSubscriptionId() {
    $controller = $this->getWebhookControllerForTestProcessor();
    try {
      $controller->getContributionRecurFromSubscriptionId('');
      $this->fail("Expected CRM_GoCardless_WebhookEventIgnoredException");
    }
    catch (\CRM_GoCardless_WebhookEventIgnoredException $e) {
      $this->assertEquals('ERROR: No subscription_id data passed into getContributionRecurFromSubscriptionId', $e->getMessage());
    }
    try {
      $controller->getContributionRecurFromSubscriptionId('doesnotexist');
      $this->fail("Expected CRM_GoCardless_WebhookEventIgnoredException");
    }
    catch (\CRM_GoCardless_WebhookEventIgnoredException $e) {
      $this->assertEquals('ERROR: No matching recurring contribution record for processor_id doesnotexist', $e->getMessage());
    }
  }
  /**
   * According to issue 59, CiviCRM sets up 'weekly' memberships by passing
   * in a 7 day interval, which GC does not allow.
   *
   * This creates a contact with a contribution and a ContributionRecur in the
   * same way that CiviCRM's core Contribution Pages form does, then, having
   * mocked the GC API it calls
   * CRM_GoCardlessUtils::completeRedirectFlowCiviCore()
   * and checks that the result is updated contribution and ContributionRecur records.
   *
   */
  public function testIssue59() {
    // We need to mimick what the contribution page does, which AFAICS does:

    list($contactID, $recur, $contrib, $membership) = $this->createFixture([
      'recur' => [
        // Issue59
        'frequency_unit' => "day",
        'frequency_interval' => 7,
      ],
      'membership' => [],
    ]);

    // Mock the GC API.
    $this->mockGoCardlessApiForTestPaymentProcessor1($contactID, 0, $recur, NULL, 'weekly');

    // ------------
    // Call the thing we want to test
    // Params are usually assembled by the civicrm_buildForm hook.
    $params = [
      'test_mode' => TRUE,
      'redirect_flow_id' => 'RE1234',
      'session_token' => 'aabbccdd',
      'contactID' => $contactID,
      'description' => 'test contribution',
      'contributionID' => $contrib['id'],
      'contributionRecurID' => $recur['id'],
      'membershipID' => $membership['id'],
      'payment_processor_id' => $this->test_mode_payment_processor['id'],
      'entryURL' => 'http://example.com/somwhere',
    ];
    CRM_GoCardlessUtils::completeRedirectFlowCiviCore($params);

    // Now test the contributions were updated.
    $recur = civicrm_api3('ContributionRecur', 'getsingle', ['id' => $recur['id']]);

    // This would fail due to issue59.
    $this->assertContributionRecurStatus($recur, 'In Progress');
    $this->assertEquals('SUBSCRIPTION_ID', $recur['processor_id']);
    $this->assertEquals("$this->defaultDateSubscriptionStarts 00:00:00", $recur['start_date']);

    $result = civicrm_api3('Contribution', 'getsingle', ['id' => $contrib['id']]);
    $this->assertEquals("$this->defaultDateSubscriptionStarts 00:00:00", $result['receive_date']);
    $this->assertEquals(2, $result['contribution_status_id']);
    $result = civicrm_api3('Membership', 'getsingle', ['id' => $membership['id']]);
    // status should be still be Pending
    $this->assertEquals($this->membership_status_map["Pending"], $result['status_id']);
    // Dates should be unchanged
    foreach (['start_date', 'end_date', 'join_date'] as $date) {
      $this->assertEquals($membership[$date] ?? NULL, $result[$date] ?? NULL);
    }
  }

  public function testUpgrade0002() {
    $payment_instrument = civicrm_api3('OptionValue', 'getsingle', [ 'option_group_id' => "payment_instrument", 'name' => "direct_debit_gc" ])['value'];
    $processor_type = civicrm_api3('PaymentProcessorType', 'getsingle', [ 'name' => 'GoCardless', 'options' => ['limit' => 0] ])['id'];

    // After an install, our processor should be correctly set up.
    $proc = civicrm_api3('PaymentProcessor', 'getsingle', ['id' => $this->test_mode_payment_processor['id']]);
    $this->assertEquals($payment_instrument, $proc['payment_instrument_id']);
    $this->assertEquals(CRM_Core_Payment::PAYMENT_TYPE_DIRECT_DEBIT, $proc['payment_type']);

    // Now bodge this backwards.
    CRM_Core_DAO::executeQuery("UPDATE civicrm_payment_processor SET payment_type=1, payment_instrument_id=1 WHERE id = %1", [
      1 => [$this->test_mode_payment_processor['id'], 'Integer']
    ]);

    // Now run the upgrade
    $up = new CRM_GoCardless_Upgrader(E::LONG_NAME, E::path());
    $up->upgrade_0002();

    // And re-test
    $proc = civicrm_api3('PaymentProcessor', 'getsingle', ['id' => $this->test_mode_payment_processor['id']]);
    $this->assertEquals($payment_instrument, $proc['payment_instrument_id']);
    $this->assertEquals(CRM_Core_Payment::PAYMENT_TYPE_DIRECT_DEBIT, $proc['payment_type']);

  }
  /**
   * check sending receipts.
   *
   * Variables: contrib recur, contrib API, sendReceiptsForCustomPayments
   *
   * Note: there are several places where sending receipts is controlled.
   *
   * is_send_contribution_notification
   * is_email_receipt
   *
   * @dataProvider dataForTestSendReceipts
   */
  public function testSendReceipts($policy, $recur_is_email_receipt, $expected) {
    $mut = new CiviMailUtils($this, TRUE);

    $settings = CRM_GoCardlessUtils::getSettings();
    $settings['sendReceiptsForCustomPayments'] = $policy;
    Civi::settings()->set('gocardless', json_encode($settings));

    $fixtureOverrides = [
      'recur' => [
        'processor_id' => 'SUBSCRIPTION_ID', /* simulate a complete redirect flow */
        'is_email_receipt' => $recur_is_email_receipt,
      ],
      'order' => [],
    ];
    list($contactID, $recur, $contrib, $membership) = $this->createFixture($fixtureOverrides, FALSE);

    // Mock webhook input data.
    $body = '{"events":[
      {"id":"EV1","resource_type":"payments","action":"confirmed","links":{"payment":"PAYMENT_ID"}}
      ]}';
    $calculatedSignature = $this->mockGoCardlessApiForWebhook($body, '{
        "id":"PAYMENT_ID",
        "status":"confirmed",
        "charge_date": "2016-10-02",
        "amount":5000,
        "links":{"subscription":"SUBSCRIPTION_ID"}
      }'
    );

    // Trigger webhook
    $controller = new CRM_Core_Payment_GoCardlessIPN();
    $controller->parseWebhookRequest(["Webhook-Signature" => $calculatedSignature], $body);
    $controller->processWebhookEvents(TRUE);

    $contrib = civicrm_api3('Contribution', 'getsingle', ['id' => $contrib['id']]);
    $this->assertContributionStatus($contrib, 'Completed');
    $messagesSent = count($mut->getAllMessages());

    if ($expected) {
      // Check that a receipt WAS sent.
      $this->assertNotEmpty($contrib['receipt_date'] ?? NULL);
      $this->assertEquals(1, $messagesSent, "Expected 1 email to be sent when first payment completed. (receipt date: ${contrib['receipt_date']})");
      $mut->checkMailLog(['Contribution Information']);
    }
    else {
      // Check it was NOT sent.
      $this->assertEmpty($contrib['receipt_date'] ?? NULL);
      $this->assertEquals(0, $messagesSent, "Expected no emails to be sent when first payment completed");
      $mut->checkMailLog([], ['Contribution Information']);
    }

    // Subsequent payments
    //
    // Reset mail log
    $mut->clearMessages();

    // Create 2nd contrib.
    // Mock webhook input data.
    $body = '{"events":[
      {"id":"EV2","resource_type":"payments","action":"confirmed","links":{"payment":"PAYMENT_ID_2"}}
      ]}';
    $calculatedSignature = $this->mockGoCardlessApiForWebhook($body, '{
        "id":"PAYMENT_ID_2",
        "status":"confirmed",
        "charge_date": "2016-11-02",
        "amount":5000,
        "links":{"subscription":"SUBSCRIPTION_ID"}
      }', 'PAYMENT_ID_2');

    // Trigger webhook
    $controller = new CRM_Core_Payment_GoCardlessIPN();
    $controller->parseWebhookRequest(["Webhook-Signature" => $calculatedSignature], $body);
    $controller->processWebhookEvents(TRUE);

    // Find the latest contribution.
    $contribution2 = civicrm_api3('Contribution', 'get', [
      'contribution_recur_id' => $recur['id'],
      'return' => ['receipt_date', 'receive_date'],
      'sequential' => 1,
      'is_test' => 1,
      'options' => ['limit' => 1, 'sort' => "id DESC"]])['values'][0] ?? NULL;
    // Check that this is a different contrib to the other one.
    $this->assertNotNull($contribution2, "Expected to find a contribution but got none!");
    $this->assertNotEquals($contrib['id'], $contribution2['id'], 'Expected for a new contribution to have been added but seems the last one is the first one we made.');
    $this->assertEquals('2016-11-02 00:00:00', $contribution2['receive_date'], 'Wrong receive_date on followup contribution');
    $receipt_date = $contribution2['receipt_date'] ?? NULL;
    $messagesSent = count($mut->getAllMessages());
    if ($expected) {
      // Check that a receipt WAS sent.
      $this->assertNotEmpty($receipt_date, 'Expected a receipt_date to have been set for subsequent payment');
      $this->assertEquals(1, $messagesSent, "Expected one email to be sent for subsequent payments");
      $mut->checkMailLog(['Contribution Information', 'PAYMENT_ID_2']);
      // We only expect one message
      $this->assertEquals(1, $messagesSent);
    }
    else {
      // Check it was NOT sent.
      $this->assertEmpty($receipt_date, 'Expected a receipt_date NOT to have been set for subsequent payment');
      $mut->checkMailLog([], ['Contribution Information', 'PAYMENT_ID_2']);
      // We don't expect any messages
      $this->assertEquals(0, $messagesSent, "Expected no emails to be sent for subsequent payments");
    }
    $mut->stop();
    $mut->clearMessages();

  }
  /**
   * Data provider for testSendReceipts
   */
  public function dataForTestSendReceipts() {
    return [
      [ 'always', 0, 1 ],
      [ 'always', 1, 1 ],
      [ 'never', 0, 0 ],
      [ 'never', 1, 0 ],
      [ 'defer', 0, 0 ],
      [ 'defer', 1, 1 ],
    ];
  }
  /**
   */
  public function testGocardlessfailabandoned() {
    $dt = new DateTimeImmutable();
    // Create an old contribution date.
    $receive_date = $dt->modify("-1 day")->format("Y-m-d");

    $fixtureOverrides = [
      'recur' => [
        'processor_id' => 'SUBSCRIPTION_ID', /* simulate a complete redirect flow */
        'amount' => 50,
        'start_date' => $receive_date,
      ],
      'order' => [
        'receive_date' => $receive_date,
      ],
    ];
    list($contactID, $recur, $contrib, $membership) = $this->createFixture($fixtureOverrides, FALSE);

    civicrm_api3('Job', 'Gocardlessfailabandoned', ['contribution_recur_id' => $recur['id']]);

    // We expect that the contrib recur is now Failed
    // We expect that the contrib is now Cancelled
    $recur = civicrm_api3('ContributionRecur', 'getsingle', ['id' => $recur['id']]);
    $this->assertContributionRecurStatus($recur, 'Failed');

    $contrib = civicrm_api3('Contribution', 'getsingle', ['id' => $contrib['id']]);
    $this->assertContributionStatus($contrib, 'Cancelled');
  }
  protected function createExistingMembershipNotPaidByGC($contactID, ?array $membershipParamsOverride, $expectedStatus = 'New') {

    $dt = new DateTimeImmutable();

    // Create a membership not using GC
    $contrib = civicrm_api3('Order', 'create',
      [
        'sequential' => 1,
        'financial_type_id' => 1,
        'total_amount' => 1,
        'receive_date' => $membershipParamsOverride['start_date'],
        'contact_id' => $contactID,
        'contribution_status_id' => "Pending",
        'is_test' => 1,
        'line_items' => [[
          'params' => $membershipParamsOverride + [
            'membership_type_id' => 'MyMembershipType',
            'contact_id' => $contactID,

            // Needed to override default status calculation *deprecated*
            'skipStatusCal' => 1,
          ],
          'line_item' => [[
            'entity_table' => 'civicrm_membership',
            'line_total' => 1,
            'unit_price' => 1,
            "price_field_id" => 1,
            "price_field_value_id" => 1, // xxx ?
            'financial_type_id' => 1, /* 'Donation' */
            'qty' => 1,
          ]]
        ]
      ],
    ]);
    $contrib = civicrm_api3('Order', 'getsingle', ['id' => $contrib['id']]);
    $membership = civicrm_api3('Membership', 'getsingle', ['id' => $contrib['line_items'][0]['entity_id']]);

    //print json_encode(['input' => $membershipParamsOverride, 'output' => $membership ], JSON_PRETTY_PRINT);

    // Now get that membership started with a payment.
    civicrm_api3('Payment', 'create', [
        'contribution_id' => $contrib['id'],
        'total_amount' => $contrib['total_amount'],
        'trxn_date' => $contrib['receive_date'],
        'trxn_id' => 'NON_GC_PAYMENT_ID',
        'is_send_contribution_notification' => 0,
      ]);
    // Reload membership and contrib
    $contrib = civicrm_api3('Order', 'getsingle', ['id' => $contrib['id']]);
    $membership = civicrm_api3('Membership', 'getsingle', ['id' => $membership['id']]);
    // Check it's what we expect - this is a sanity check, it's not actually checking GC stuff.
    $this->assertMembershipStatus($membership, $expectedStatus);
    $this->assertContributionStatus($contrib, 'Completed');

    //print json_encode(['input' => $membershipParamsOverride, 'output' => $membership ], JSON_PRETTY_PRINT);

    return [$contrib, $membership];
  }
  /**
   * Return a fake GoCardless IPN processor.
   *
   * Helper function for other tests.
   */
  protected function getWebhookControllerForTestProcessor() :CRM_Core_Payment_GoCardlessIPN {
    $pp_config = $this->test_mode_payment_processor;
    $pp = Civi\Payment\System::singleton()->getByProcessor($pp_config);
    $controller = new CRM_Core_Payment_GoCardlessIPN($pp);
    return $controller;
  }

  /**
   * Helper
   */
  protected function mockGoCardlessApiForTestPaymentProcessor($mock) {
    $obj = new CRM_Core_Payment_GoCardless('test', $this->test_mode_payment_processor);
    $obj->setGoCardlessApi($mock);
  }

  /**
   * DRY code.
   *
   * @param string $paymentJSON
   */
  protected function mockPaymentGet($paymentJSON) {
    $payment = json_decode($paymentJSON);
    if (!$payment) {
      throw new \InvalidArgumentException("test code error: \$paymentJSON must be valid json but wasn't");
    }
    $this->mockGoCardlessApiForWebhook('', $paymentJSON);
  }
  /**
   * Fetch the last contribution (by ID) for the given recurID.
   *
   * @param int $recurID
   *
   * @return array
   */
  protected function getLatestContribution($recurID) {
    $r = civicrm_api3('Contribution', 'get', [
      'contribution_recur_id' => $recurID,
      'sequential' => 1,
      'is_test' => 1,
      'options' => ['limit' => 0, 'sort' => "id ASC"],
    ])['values'] ?? [];
    if ($r) {
      return end($r);
    }
  }
  protected function createTestContact():int {
    if (empty($this->contactID)) {
      $contact = civicrm_api3('Contact', 'create', [
        'sequential' => 1,
        'contact_type' => "Individual",
        'first_name' => "Wilma",
        'last_name' => "Flintstone",
        'email' => 'wilma@example.org',
      ]);
      $this->contactID = (int) $contact['id'];
    }
    return $this->contactID;
  }
  protected function assertMembershipStatus(array $membership, string $statusName) {
    $expectedStatusID = $this->membership_status_map[$statusName];
    $gotStatusName = $this->membership_status_map_flip[$membership['status_id']];
    $this->assertEquals($expectedStatusID, $membership['status_id'],
      "Expected memberships status '$statusName' but got '$gotStatusName'"
    );
  }
  protected function assertContributionStatus(array $contrib, string $statusName) {

    $expectedStatusID = $this->contribution_status_map[$statusName];
    $gotStatusID = $contrib['contribution_status_id'];
    $gotStatusName = array_flip($this->contribution_status_map)[$gotStatusID];
    $this->assertEquals($expectedStatusID, $gotStatusID,
      "Expected contribution status '$statusName' but got '$gotStatusName'"
    );
  }
  protected function assertContributionRecurStatus(array $recur, string $statusName) {

    $expectedStatusID = $this->contribution_recur_status_map[$statusName];
    $gotStatusID = $recur['contribution_status_id'];
    $gotStatusName = array_flip($this->contribution_status_map)[$gotStatusID];
    $this->assertEquals($expectedStatusID, $gotStatusID,
      "Expected contribution recur status '$statusName' but got '$gotStatusName'"
    );
  }
  /**
   * Creates a recurring contribution in the way one would be created before
   * the user is sent off-site to GC.
   *
   * i.e. Pending, no processor_id.
   * Also note that the start_date passed in would usually get changed by completeRedirectFlowCiviCore
   */
  protected function createContribRecur($overrides = []) {
    return civicrm_api3('ContributionRecur', 'create',
      $overrides + [
        'sequential' => 1,
        'contact_id' => $this->createTestContact(),
        'frequency_interval' => 1,
        'payment_processor_id' => $this->test_mode_payment_processor['id'],
        'amount' => 1,
        'frequency_unit' => "month",
        'start_date' => $this->defaultDate,
        'financial_type_id' => 1,
        'is_test' => 1,
        'contribution_status_id' => "Pending",
      ])['values'][0];
  }
  /**
   * Allow a test to implement this hook.
   *
   * This uses HookInterface but defers to a callable method stored (by a test
   * method) at $this->handleRedirectFlowHookTest
   */
  public function hook_civicrm_handleRedirectFlowWithGoCardless($details, &$result) {
    if (!$this->handleRedirectFlowHookTest) {
      // Do nothing.
      return;
    }
    $callable = $this->handleRedirectFlowHookTest;
    $callable($details, $result);
  }
}
