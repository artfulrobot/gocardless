<?php
/**
 * @file
 * Exception for when an event in a webhook cannot be processed.
 */
class CRM_GoCardless_WebhookEventIgnoredException extends \Exception {

  protected string $logLevel;
  /**
   * Re use the exception's 'code' prop with a PEAR_LOG_* level.
   */
  public function __construct(string $message, string $logLevel = 'error') {
    parent::__construct($message);
    $this->logLevel = $logLevel;
  }

  public function getLogLevel() :string {
    return $this->logLevel;
  }

  /**
   */
  public function isOk() :bool {
    return in_array($this->logLevel, ['debug', 'info', 'notice']);
  }

  /**
   * ERROR: PEAR_LOG_WARNING, PEAR_LOG_ERR
   */
  public function isError() :bool {
    return !$this->isOk();
  }
}
