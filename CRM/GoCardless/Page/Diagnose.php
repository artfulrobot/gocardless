<?php
use CRM_GoCardless_ExtensionUtil as E;

class CRM_GoCardless_Page_Diagnose extends CRM_Core_Page {

  public function run() {
    // Example: Set the page-title dynamically; alternatively, declare a static title in xml/Menu/*.xml
    CRM_Utils_System::setTitle(E::ts('Diagnose'));

    $entities = trim($_GET['entities'] ?? '');
    $debug = ($_GET['debug'] ?? '') === '1';

    $this->assign('mermaid', '');
    $this->assign('debugLog', '');
    $this->assign('entitiesList', '');
    $this->assign('doNotLoadAllCnFromCt', !empty($_GET['doNotLoadAllCnFromCt']));
    $this->assign('doNotLoadAllCrFromCt', !empty($_GET['doNotLoadAllCrFromCt']));

    if ($entities) {
      $diagnose = new Civi\GoCardless\Diagnose($debug);
      if (!empty($_GET['doNotLoadAllCnFromCt'])) {
        $diagnose->doNotLoadAllCnFromCt();
      }
      if (!empty($_GET['doNotLoadAllCrFromCt'])) {
        $diagnose->doNotLoadAllCrFromCt();
      }
      $diagnose->loadEntities($entities);
      $out = $diagnose->generate();
      $this->assign('mermaid', $out['mermaid']);
      $this->assign('warning', $out['warning'] ?? '');
      // Fetch safe version of entitiesList
      $this->assign('entitiesList', $diagnose->entitiesList);
      $this->assign('debugLog', $diagnose->getLogsHTML());
    }
    $this->assign('mermaidUrl', E::url('js/mermaid.min.js'));

    parent::run();
  }

}
