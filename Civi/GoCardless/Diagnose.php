<?php
namespace Civi\GoCardless;

class Diagnose
{
  protected $things = [];
  protected $added = 0;
  protected $debug = FALSE;
  protected $log = [];
  protected $loadAllCnFromCt = TRUE;
  protected $loadAllCrFromCt = TRUE;
  public string $entitiesList = '';

  public function __construct(bool $debug = FALSE) {

    $this->things = [
      'ct' => [],
      'cn' => [],
      'cr' => [],
      'm' => [],
      'li' => [],
    ];

    $this->debug = $debug;
  }
  public function doNotLoadAllCnFromCt() {
    $this->loadAllCnFromCt = FALSE;
    return $this;
  }

  public function doNotLoadAllCrFromCt() {
    $this->loadAllCrFromCt = FALSE;
    return $this;
  }

  public function loadEntities(string $entitiesList) {

    if ($entitiesList) {
      $args = preg_split('/\s+/', trim($entitiesList));
      foreach ($args as $v) {
        if (preg_match('/^(cn|ct|m|cr)(\d+)$/', $v, $matches)) {
          $this->loadByID($matches[1], [(int) $matches[2]]);
        }
        else {
          throw new \InvalidArgumentException("Invalid input item: " . $v);
        }
      }
      // If we get here, entitiesList is safe.
      $this->entitiesList = implode(' ', $args);
    }

    return $this;
  }

  public function generate() {
    $x = 10;

    do {
      $this->log("Pass " . (11 - $x));
      $this->added = 0;
      $this->loadRelatedRecurs();
      $this->loadRelatedContributions();
      $this->loadRelatedMemberships();
      $this->loadRelatedLineItems();
      //echo "...added $this->added\n";
      $x--;
    } while ($this->added > 0 && $x);

    if (!$x) {
      $warning = "Fetching entities took 10 iterations; it’s possible an infinite loop was occurring - this would be a bug. Add &debug=1 to check.";
    }


    // Create mermaid.
    $s = ',stroke:none';
    $styling = ["classDef ct fill:#acd05d$s", "classDef m fill:#ffcb2f$s", "classDef cn fill:#6eb590$s", "classDef cr fill:#ff9359$s", "classDef li fill:#cae3e0$s"];
    $mermaid = [];
    $link = 0;
    foreach ($this->things['ct'] as $id => $contact) {
       $mermaid[] = "ct$id(\"Contact $id<br>$contact[first_name]\"):::ct";
    }
    foreach ($this->things['cr'] as $id => $cr) {
      $endDate = empty($cr['end_date'])
      ? (
        empty($cr['cancel_date'])
        ? '?'
        : $cr['cancel_date'])
      : '?';

      $mermaid[] = "cr$id" . "(\"Recur $id<br>£$cr[amount] " . $cr['contribution_status_id:name'] . "<br>" . substr($cr['start_date'], 0, 10) . " - $endDate\"):::cr";
      $mermaid[] = "cr$id -.-> ct" . $cr['contact_id'];
      $styling[] = 'linkStyle ' . ($link++) . ' stroke:#acd05d';
    }
    foreach ($this->things['cn'] as $id => $cn) {
      $mermaid[] = "cn$id" . "(\"Contribution " . "$id<br>£$cn[total_amount] " . substr($cn['receive_date'], 0, 10) . "<br>" . $cn['contribution_status_id:name'] . "\"):::cn";
      $mermaid[] = "cn$id -.-> ct" . $cn['contact_id'];
      $styling[] = 'linkStyle ' . ($link++) . ' stroke:#acd05d';
      if ($cn['contribution_recur_id']) {
        $mermaid[] = "cn$id --> cr" . $cn['contribution_recur_id'];
        $styling[] = 'linkStyle ' . ($link++) . ' stroke:#ff9359';
      }
      if ($cn['mc']) {
        // Nb. -.-> is supposed to be a dotted line but it doesn't turn out like that for me, so apply the dasharray
        $mermaid[] = "cn$id -.-> m" . $cn['mc'];
        $styling[] = 'linkStyle ' . ($link++) . ' stroke:#ffcb2f,stroke-dasharray:16 3;';
      }
    }
    foreach ($this->things['m'] as $id => $m) {
      $mermaid[] = "m$id(\"Membership $id<br>" . $m['membership_type_id:name'] . "<br>" . $m['status_id:name'] . " " . substr($m['start_date'], 0, 10) . " - " . substr($m['end_date'], 0, 10) . "\"):::m";
      $mermaid[] = "m$id -.-> ct" . $cn['contact_id'];
      $styling[] = 'linkStyle ' . ($link++) . ' stroke:#acd05d';
      if ($m['contribution_recur_id']) {
        $mermaid[] = "m$id --> cr" . $m['contribution_recur_id'];
        $styling[] = 'linkStyle ' . ($link++) . ' stroke:#ff9359';
      }
    }

    foreach ($this->things['li'] as $id => $li) {
      $mermaid[] = "li$id(\"LineItem $id<br>$li[line_total] $li[entity_table]\"):::li";
      $mermaid[] = "li$id --> cn" . $li['contribution_id'];
      $styling[] = 'linkStyle ' . ($link++) . ' stroke:#6eb590';
      if ($li['entity_table'] === 'civicrm_membership') {
        $mermaid[] = "li$id --> m" . $li['entity_id'];
        $styling[] = 'linkStyle ' . ($link++) . ' stroke:#ffcb2f';
      }
    }

    return [
      'mermaid' =>
        // '%%{init: {"flowchart": { "diagramPadding": 200 }}}%%' . "\n  "
        // '%%{init: {flowchart: { diagramPadding: 200 }}}%%' . "\n  "
        //'%%{init: {"flowchart": {"diagramPadding": 200}} }%%' . "\n"
        //'%%{init: {"flowchart": {"diagramPadding": 200}} }%%' . "\n"
         "graph TD\n"
        . implode("\n  ", array_merge($mermaid, $styling)) . "\n",
      'warning' => $warning ?? '',
    ];
  }
  public function loadByID($what, $ids, string $log = '') {
    if (!$ids) {
      return;
    }
    switch ($what){
    case 'cn':
      $api = \Civi\Api4\Contribution::get(FALSE)
        ->addSelect('contribution_status_id:name', 'receive_date', 'total_amount', 'contact_id', 'contribution_recur_id')
      ;
      break;

    case 'ct':
      $api = \Civi\Api4\Contact::get(FALSE)
        ->addSelect('first_name')
      ;
      break;

    case 'cr':
      $api = \Civi\Api4\ContributionRecur::get(FALSE)
        ->addSelect('contribution_status_id:name', 'start_date', 'end_date', 'cancel_date', 'contact_id', 'amount')
      ;
      break;

    case 'm':
      $api = \Civi\Api4\Membership::get(FALSE)
        ->addSelect('membership_type_id:name', 'status_id:name', 'start_date', 'end_date', 'contribution_recur_id')
      ;
      break;

    default:
      throw new \InvalidArgumentException("$what?");
    }
    $api->addWhere('id', 'IN', $ids);
    $result = $api->execute()->indexBy('id');
    $this->merge($what, $result->getArrayCopy(), $log);
  }
  public function loadRelatedRecurs() {
    $this->log('loadRelatedRecurs ' . $this->added);

    // Pull in recurs referenced by loaded contributions
    $ids = [];
    foreach ($this->things['cn'] as $row) {
      if (!empty($row['contribution_recur_id'])) {
        $ids[] = $row['contribution_recur_id'];
      }
    }
    $ids = array_diff($ids, array_keys($this->things['cr']));
    $this->loadByID('cr', $ids, 'loadRelatedRecurs: from contributions');

    // Pull in recurs referenced by loaded memberships
    $ids = [];
    foreach ($this->things['m'] ?? [] as $row) {
      if (!empty($row['contribution_recur_id'])) {
        $ids[] = $row['contribution_recur_id'];
      }
    }
    $ids = array_diff($ids, array_keys($this->things['cr']));
    $this->loadByID('cr', $ids, 'loadRelatedRecurs: from memberships');

    // Pull in all recurs owned by contact.
    $ids = array_keys($this->things['ct']);
    if ($this->loadAllCrFromCt && $ids) {
      $api = \Civi\Api4\ContributionRecur::get(FALSE)
        ->addSelect('contribution_status_id:name', 'start_date', 'end_date', 'cancel_date', 'contact_id', 'amount')
        ->addWhere('contact_id', 'IN', $ids);
      if (!empty($this->things['cr'])) $api->addWhere('id', 'NOT IN', array_keys($this->things['cr']));
      $result = $api->execute()->indexBy('id');
      $this->merge('cr', $result->getArrayCopy(), 'loadRelatedRecurs: recurs referenced by contributions');
    }
    $this->log('loadRelatedRecurs ends ' . $this->added);
  }
  public function loadRelatedContributions() {
    $this->log('loadRelatedContributions ' . $this->added);

    // Load contributions that belong to loaded recurs.
    $ids = array_keys($this->things['cr']);
    if ($ids) {
      $api = \Civi\Api4\Contribution::get(FALSE)
        ->addSelect('contribution_status_id:name', 'receive_date', 'total_amount', 'contact_id', 'contribution_recur_id')
        ->addWhere('contribution_recur_id', 'IN', $ids);
      if (!empty($this->things['cn'])) {
        $api->addWhere('id', 'NOT IN', array_keys($this->things['cn']));
      }
      $result = $api->execute()->indexBy('id');
      $this->merge('cn', $result->getArrayCopy(), 'loadRelatedContributions belonging to loaded recurs');
    }

    // Load contributions that belong to loaded contacts.
    $ids = array_keys($this->things['ct']);
    if ($ids && $this->loadAllCnFromCt) {
      $api = \Civi\Api4\Contribution::get(FALSE)
        ->addSelect('contribution_status_id:name', 'receive_date', 'total_amount', 'contact_id', 'contribution_recur_id')
        ->addWhere('contact_id', 'IN', $ids);
      if ($this->things['cn']) {
        $api->addWhere('id', 'NOT IN', array_keys($this->things['cn']));
      }
      $result = $api->execute()->indexBy('id');
      $this->merge('cn', $result->getArrayCopy(), 'loadRelatedContributions belonging to contacts');
    }

    // Load membership payments for our contributions.
    $ids = [];
    foreach ($this->things['cn'] as $cn) {
      if (!isset($cn['mc'])) {
        // We have not yet looked up whether there's an mc record for this.
        $ids[] = $cn['id'];
      }
    }
    if ($ids) {
      $cnIDTomID = [];
      foreach ($ids as $id) {
        $result = civicrm_api3('MembershipPayment', 'get', [
          'contribution_id' => $id,
          'options' => ['limit' => 0],
        ]);
        foreach($result['values'] ?? [] as $row) {
          $cnIDTomID[$row['contribution_id']] = $row['membership_id'];
        }
      }
      foreach ($ids as $cnID) {
        $this->added++;
        $this->things['cn'][$cnID]['mc'] = $cnIDTomID[$cnID] ?? 0;
      }
      $this->log('loadRelatedContributions updated membership links for '. count($ids));
    }

    // Load all contacts for our contributions
    $ids = [];
    foreach ($this->things['cn'] as $cn) {
      $contactID = $cn['contact_id'];
      if (!isset($this->things['ct'][$contactID])) {
        $ids[] = $contactID;
      }
    }
    $this->loadByID('ct', $ids, 'loadRelatedContributions load contacts referenced by contributions');
  }
  public function loadRelatedMemberships() {
    // Load all memberships for the contacts.
    $ids = array_keys($this->things['ct']);
    if ($ids) {
      $api = \Civi\Api4\Membership::get(FALSE)
        ->addSelect('membership_type_id:name', 'status_id:name', 'start_date', 'end_date', 'contribution_recur_id')
        ->addWhere('contact_id', 'IN', $ids);
      if ($this->things['m']) {
        $api->addWhere('id', 'NOT IN', array_keys($this->things['m']));
      }
      $result = $api->execute()->indexBy('id');
      $this->merge('m', $result->getArrayCopy(), 'loadRelatedMemberships load memberships for contacts');
    }

    // Load all memberships referenced by Contributions.
    $ids = [];
    foreach ($this->things['cn'] as $row) {
      if ($row['mc'] ?? 0) {
        $ids[] = $row['mc'];
      }
    }
    $ids = array_diff($ids, array_keys($this->things['m']));
    $this->loadByID('m', $ids, 'loadRelatedMemberships load memberships referenced by contributions’ membership-payments.');

    // Load all memberships that reference loaded CRs.
    $ids = array_keys($this->things['cr']);
    if ($ids) {
      $api = \Civi\Api4\Membership::get(FALSE)
        ->addSelect('membership_type_id:name', 'status_id:name', 'start_date', 'end_date', 'contribution_recur_id')
        ->addWhere('contribution_recur_id', 'IN', $ids);
      if ($this->things['m']) {
        $api->addWhere('id', 'NOT IN', array_keys($this->things['m']));
      }
      $result = $api->execute()->indexBy('id');
      $this->merge('m', $result->getArrayCopy(), 'loadRelatedMemberships load memberships reference by CRs.');
    }

    $ids = [];
    foreach ($this->things['li'] as $row) {
      if ($row['entity_table'] === 'civicrm_membership') {
        $ids[] = (int) $row['entity_id'];
      }
    }
    $ids = array_diff($ids, array_keys($this->things['m']));
    $this->loadByID('m', $ids, 'loadRelatedMemberships referenced by line items');

  }
  public function loadRelatedLineItems() {
    $this->log('loadRelatedLineItems ' . $this->added);

    $ids = array_keys($this->things['cn']);
    if ($ids) {
      $api = \Civi\Api4\LineItem::get(FALSE)
        ->addSelect('entity_table', 'entity_id', 'contribution_id', 'line_total')
        ->addWhere('contribution_id', 'IN', $ids);
      if ($this->things['li']) {
        $api->addWhere('id', 'NOT IN', array_keys($this->things['li']));
      }
      $result = $api->execute()->indexBy('id');
      $this->merge('li', $result->getArrayCopy(), 'loadRelatedLineItems for loaded contributions');
    }

  }
  public function merge(string $what, array $result, string $log = '') {
    foreach ($result as $row) {
      $this->added++;
      $this->things[$what][$row['id']] = $row;
    }
    if ($log) {
      $this->log($log . " merged " . count($result) . " $what records");
    }
  }

  public function log(string $message) {
    if ($this->debug) {
      $this->log[] = $message;
    }
  }
  public function getLogsHTML() :string {
    return $this->debug ? ("<ul><li>" . implode('</li><li>', array_map('htmlspecialchars', $this->log)) . '</li></ul>') : '';
  }
}
