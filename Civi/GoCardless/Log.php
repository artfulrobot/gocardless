<?php
namespace Civi\GoCardless;

/**
 * This logger adds a few features to the standard log, while preserving psr3 a bit.
 *
 */
class Log extends \Psr\Log\AbstractLogger {

  protected static $singletons;

  protected Array $labelStack;

  protected string $prefix = '';

  public string $logFile = '';

  public array $testingLog;

  /**
   * Singletons for each log prefix.
   */
  public static function ger(string $prefix = 'default') {
    if (!isset(static::$singletons)) {
      static::$singletons = [];
    }
    if (!isset(static::$singletons[$prefix])) {
      static::$singletons[$prefix] = new static($prefix);
    }
    return static::$singletons[$prefix];
  }

  /**
   * Constructor
   */
  public function __construct(string $prefix = 'default') {
    $this->labelStack = [];
    $this->prefix = $prefix;

    \CRM_Core_Error::createDebugLogger(($prefix === 'default') ? '' : $prefix);
    $this->logFile = \Civi::$statics['CRM_Core_Error']['logger_file' . $prefix];
  }

  /**
   * Support:
   * - {placeholders}
   * - Appending JSON version of context to message.
   * - Context can include the following meta-meta data that are consumed by
   *   this function and removed from the array:
   *   - '=start' => 'label' Begin a group 'label'
   *   - '=pop' =>1 End 1 group(s)
   *   - '=set' => [] Replace all groups
   *   - '=compact' don't use JSON_PRETTY_PRINT when appending $context.
   */
  public function log($level, $message, array $context = []) {
    $levelInteger = \CRM_Core_Error_Log::getMap()[$level] ?? FALSE;
    if ($levelInteger === FALSE) {
      throw new \Psr\Log\InvalidArgumentException("Invalid log level identifier '$level'. Try debug,info,notice,warning,error,alert,critical,emergency");
    }

    $labels = (Array) ($context['=start'] ?? []);
    $this->labelStack = array_merge($this->labelStack, array_values($labels));

    if (isset($context['=set'])) {
      $this->labelStack = (Array) ($context['=set'] ?? []);
    }
    $jsonOpts = JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | ((isset($context['=compact'])) ? 0 : JSON_PRETTY_PRINT);

    $endGroups = (int) ($context['=pop'] ?? 0);
    unset($context['=start'], $context['=pop'], $context['=set'], $context['=compact']);

    $message = trim((string) $message);
    if ($message || $context) {

      // Handle placeholders.
      $closeCurly = "\125";
      $message = preg_replace_callback("/{([^$closeCurly]+?)}/", function($m) use ($context) {
          if (!array_key_exists($m[1], $context)) {
            // Leave the original in place.
            return $m[0];
          }
          else {
            return json_encode($context[$m[1]], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
          }
        }, $message);

      // Format context.
      if ($context) {
        // Format exceptions.
        if (isset($context['exception'])) {
          $context['exception'] = \CRM_Core_Error::formatTextException($context['exception']);
        }
        $message .= "\n" . json_encode($context, $jsonOpts);
      }

      $beforeMessage = date('c') . " [$level] ";

      // Add labelStack
      if ($this->labelStack) {
        $beforeMessage .= ':' . implode(':', $this->labelStack) . ': ';
      }

      file_put_contents($this->logFile, "$beforeMessage$message\n", FILE_APPEND);

      if (isset($this->testingLog)) {
        // Give phpunit tests an easy way to check logs. Nb. call enableTesting() first.
        $this->testingLog[] = "$beforeMessage$message\n";
      }
    }

    // Apply pop.
    while ($endGroups-- > 0) {
      array_pop($this->labelStack);
    }
  }

  public function debug($message, array $context = []) {
    $this->log('debug', $message, $context);
  }

  public function info($message, array $context = []) {
    $this->log('info', $message, $context);
  }

  public function notice($message, array $context = []) {
    $this->log('notice', $message, $context);
  }

  public function warning($message, array $context = []) {
    $this->log('warning', $message, $context);
  }

  public function error($message, array $context = []) {
    $this->log('error', $message, $context);
  }

  public function critical($message, array $context = []) {
    $this->log('critical', $message, $context);
  }

  public function alert($message, array $context = []) {
    $this->log('alert', $message, $context);
  }

  public function emergency($message, array $context = []) {
    $this->log('emergency', $message, $context);
  }
  /**
   * Supports phpunit tests.
   */
  public function enableTesting() {
    $this->testingLog = [];
  }
}
