<p>This page can help you visualise what’s going on with Contributions, recurs, memberships.</p>
<form method=get >
  <div>
    {if $warning}
      <div class="crm-messages messages warning crm-warning who-knows-what">{$warning}</div>
    {/if}
    <label for="entities">Enter entities, space separated.</label><br/>
    <input class="crm-form-text" type=text value="{$entitiesList}" name=entities id=entities-input />
    <p>Entities are listed like: <code>&lt;type&gt;&lt;ID&gt;</code>. Use
      <code>ct</code> for contact; <code>cn</code> for contribution;
      <code>cr</code> for recurring contribution, <code>m</code> for membership.
      e.g. <code>cr123 m456</code> would show things for contribution recur 123
      and membership 456. Because it follows links recursively, you (a) probably only need to specify one thing's ID, and (b) may want to tame the output with the options below. e.g. if you’re interested in cn123 but that belongs to ct456 who has 100 other contributions, it will show all those contributions!</p>
    <label for="doNotLoadAllCnFromCt">
      <input type=checkbox class="crm-form-checkbox" type=text name=doNotLoadAllCnFromCt id=doNotLoadAllCnFromCt value=1 {if $doNotLoadAllCnFromCt}checked{/if} />
      Do not load all contributions for contacts</label>
    <br/>
    <label for="doNotLoadAllCrFromCt">
      <input type=checkbox class="crm-form-checkbox" type=text name=doNotLoadAllCrFromCt id=doNotLoadAllCrFromCt value=1 {if $doNotLoadAllCrFromCt}checked{/if} />
      Do not load all recurring contributions for contacts</label>
    <br/>
    <input type=submit value="Draw" />
  </div>
</form>

{if $mermaid}
  <div class="mermaid">{$mermaid}</div>
  <!-- script src="https://cdn.jsdelivr.net/npm/mermaid/dist/mermaid.min.js"></script -->
  <script src="https://unpkg.com/mermaid@8.14.0/dist/mermaid.min.js"></script>
  <!--script src="{$mermaidUrl}"></script>-->
  <script>
  {literal}
  mermaid.initialize({startOnLoad:true, flowchart: {diagramPadding: 20}, fontFamily: "Lato,sans-serif"});
  {/literal}
  // Force the input to update from the GET text.
  document.getElementById('entities-input').value = '{$entitiesList}';
  </script>
{/if}
{if $debugLog}
  {$debugLog}
{/if}
  <p>Generated Mermaid code (you can copy + paste this into gitlab tickets, for example.)</p>
  <textarea>```mermaid
{$mermaid|escape:"html"}
```
</textarea>

